@extends('layouts.master')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="glyphicon-show_lines"></i>
                        States
                </h3>
            </div>
            <div class="box-content">

                    @foreach(array_chunk($states->toArray(), 4) as $state_col)
                           <div class="row">
                           @foreach($state_col as $state)
                               <div class="col-md-3">
                               {{ link_to_route('admin.states.edit', $state['name'], $state['id'], ['class' => '', 'style' => 'font-size:16px; padding:5px; display:block;']) }}
                               </div>
                            @endforeach
                           </div>
                    @endforeach

            </div>
        </div>
    </div>
</div>




@stop