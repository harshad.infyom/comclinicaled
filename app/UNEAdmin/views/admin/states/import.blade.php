@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="box">

            <div class="box-title">
                <h3>
                    <i class="fa fa-cloud-upload"></i>
                    Import States
                </h3>
            </div>

            <div class="col-sm-12">
                <div class="box box-color box-bordered">
                    <div class="box-title">

                    </div>
                    <div class="box-content">
                        {{ Form::open(['route' => 'processStates', 'class' => 'form-horizontal', 'files' => 'true']) }}
                        <div class="control-group">
                            <label for="file" class="control-label">Upload Reviews</label>

                            <div class="controls">
                                <input type="file" name="file" id="file" class="input-block-level">
                                <span class="help-block">Can only import .csv files <br>(Max Size: 100MB)</span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Import Reviews</button>
                            <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div></div>

@stop