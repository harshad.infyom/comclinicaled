@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            {{ Form::model($state, ['route' => ['admin.states.update', $state->id], 'method' => 'PATCH']) }}
            <div class="box-title">
                <h3>
                    <i class="glyphicon-pushpin"></i>
                    Disciplines of {{ $state->name }}
                </h3>

                {{ Form::submit('Update', ['class' => 'btn btn-primary']) }}
                {{ link_to_route('admin.states.index', 'Back', null, ['class' => 'btn btn-primary']) }}
            </div>
            <div class="box-content">


                            @foreach(array_chunk($disciplines->toArray(), 3) as $discipline_col)

                                <div class="row">
                                    @foreach($discipline_col as $discipline)
                                    <div class="col-sm-4">
                                        {{ Form::setModel($discipline) }}
                                        {{ Form::checkbox('disciplines[]', $discipline['id'], in_array($discipline['id'], $state->disciplines->lists('id'))) }}
                                        {{ Form::label('disciplines[]', $discipline['name']) }}
                                    </div>
                                    @endforeach
                                </div>

                            @endforeach

            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


@stop