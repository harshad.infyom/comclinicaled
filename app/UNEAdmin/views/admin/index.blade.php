@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="glyphicon-circle_question_mark"></i>
                    What Would You Like To Do?
                </h3>
            </div>
            <div class="box-content">
                <ul class="tiles">
                    <li class="blue long">
                        <a href="admin/import-reviews"><span><i class="fa fa-cloud-upload"></i></span><span
                                class='name'>Import Reviews</span></a>
                    </li>
                    <li class="blue long">
                        <a href="admin/import-users"><span><i class="fa fa-upload"></i></span><span
                                class='name'>Import Users</span></a></li>

                    <li class="blue long">
                        <a href="admin/import-comments"><span><i class="fa fa-upload"></i></span><span
                                class='name'>Import Comments</span></a></li>

                    <li class="blue long">
                        <a href="admin/import-states"><span><i class="fa fa-upload"></i></span><span
                                class='name'>Import States</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@stop