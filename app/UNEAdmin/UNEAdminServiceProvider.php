<?php


namespace UNEAdmin;


use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class UNEAdminServiceProvider extends ServiceProvider {

    public function boot() {
        View::addLocation(__DIR__ . '/views');
        include __DIR__ . '/filters.php';
        include __DIR__ . '/routes.php';
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        // TODO: Implement register() method.
    }

} 