<?php


namespace UNEAdmin\Controllers;


use Input;
use Redirect;
use State;
use UNE\Models\Discipline;
use View;
use Laracasts\Flash\Flash;

class StatesController extends \BaseController {

    public function index() {
        $states = State::all();
        return View::make('admin.states.index', compact('states'));
    }

    public function edit($id){
        $state = State::find($id, ['disciplines']);
        $disciplines = Discipline::all();

        return View::make('admin.states.edit', compact('state', 'disciplines'));
    }

    public function update($id){
        $disciplines = Input::get('disciplines', []);
        //dd($disciplines);
        $state = State::find($id);
        $state->disciplines()->sync($disciplines);
        return Redirect::back();
    }

    public function create() {
        return View::make('admin.states.import');
    }

    public function import(){
        $file = Input::file('file');

        if ($file) {
            $states = State::importExcelData($file);
        } else {
            return Redirect::back()->withErrors('File not found!');
        }


        Flash::message('States imported successfully!');

        return Redirect::route('importUsers');
    }
}