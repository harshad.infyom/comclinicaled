<?php


namespace UNEAdmin\Controllers;


use Input;
use Laracasts\Flash\Flash;
use Redirect;
use View;
use Review;

class ReviewsController extends \BaseController {

    public function create() {
        return View::make('admin.reviews.import');
    }


    public function import() {
        $file    = Input::file('file');
        $reviews = Review::importExcelData($file);

        Flash::message('Imported successfully!');
        return Redirect::route('importReviews');
    }

} 