<?php


namespace UNEAdmin\Controllers;


use Input;
use Laracasts\Flash\Flash;
use Redirect;
use UserService;
use View;

class UserController extends \BaseController {
    public function create(){
        return View::make('admin.users.import');
    }

    public function import() {
        $file = Input::file('file');

        if ($file) {
            $users = UserService::importExcelData($file);
        } else {
            return Redirect::back()->withErrors('File not found!');
        }


        Flash::message('Users imported successfully!');

        return Redirect::route('importUsers');
    }


} 