<?php


namespace UNEAdmin\Controllers;


use Input;
use Laracasts\Flash\Flash;
use Redirect;
use View;
use Comment;

class CommentController extends \BaseController {

    public function create() {
        return View::make('admin.comments.import');
    }

    public function import() {
        $file = Input::file('file');

        if ($file) {
            $comments = Comment::importExcelData($file);
        } else {
            return Redirect::back()->withErrors('File not found!');
        }


        Flash::message('Users imported successfully!');

        return Redirect::route('importComments');
    }
} 