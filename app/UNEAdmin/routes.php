<?php

Route::group(['before' => 'Sentry', 'prefix' => 'admin'], function () {
    Route::get('/', ['as' => 'adminHome', 'uses' => 'UNEAdmin\Controllers\AdminController@index']);

    Route::get('import-reviews', ['as' => 'importReviews', 'uses' => 'UNEAdmin\Controllers\ReviewsController@create']);
    Route::post('import-reviews', ['as' => 'processReviews', 'uses' => 'UNEAdmin\Controllers\ReviewsController@import']);

    Route::get('import-users', ['as' => 'importUsers', 'uses' => 'UNEAdmin\Controllers\UserController@create']);
    Route::post('import-users', ['as' => 'processUsers', 'uses' => 'UNEAdmin\Controllers\UserController@import']);

    Route::get('import-comments', ['as' => 'importComments', 'uses' => 'UNEAdmin\Controllers\CommentController@create']);
    Route::post('import-comments', ['as' => 'processComments', 'uses' => 'UNEAdmin\Controllers\CommentController@import']);

    Route::get('import-states', ['as' => 'importStates', 'uses' => 'UNEAdmin\Controllers\StatesController@create']);
    Route::post('import-states', ['as' => 'processStates', 'uses' => 'UNEAdmin\Controllers\StatesController@import']);

    Route::resource('states', 'UNEAdmin\Controllers\StatesController');
});