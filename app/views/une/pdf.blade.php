<html>
<head>
</head>
<style>
    table{
        border-collapse: collapse; border: 1px solid black;
        font-family: helvetica;
        font-size: 15px;
    }
</style>
<body>
<div style="width:1140px; margin:0px auto;">
    <table width="630px" style="border: none!important;">
        <tr>
            <td colspan="2" width="630px;" style="text-align:center;border: none!important;"><img
                        src="http://nerdthemes.co/laravel/assets/img/logo.png"></td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2" width="630px;" style="border: none!important;"><p
                        style="text-align:center;padding:2px!important;text-transform: uppercase; font-size:18px;">
                    Clerkship Registration and Approval From (CRA)</p></td>
        </tr>
    </table>
    <table border="1px" width="630px;">
        <tr>
            <td colspan="2"
                style="text-align:center; background-color:#1D3365; color:#fff;"><p
                        style="color: #fff; font-weight: bold; font-size: 12px;text-align: center; padding:0px;margin: 0;text-transform: uppercase;">
                    Unecom Medical Student (Submit this Registration a MINIMUM of 45 days in Advance...)</p></td>
        </tr>
        <tr>
            <td colspan="2"><p style=" color: #000;font-size: 11px;padding: 0px 0px;text-align: center;">
                    <b>Submit this Registration A MINIMUM of 45 Days In Advance Of The Intended Start Of Your
                        Clerkship</b><br/>Clinical Education MUST approve every clerkship in advance of its start for
                    you to be covered by professional liability insurance and receive clerkship credit</p></td>
        </tr>
        <tr>
            <td style="text-align: center;background-color:#1D3365;"><span
                        style="font-size:20px;font-weight: bold; color:#fff; text-align: center;">Student Information</span>
            </td>
            <td style="text-align: center;background-color:#1D3365;"><span
                        style="font-size:20px;font-weight: bold; color:#fff; text-align: center;">Clerkship Information</span>
            </td>
        </tr>
        <tr>
            <td width="50%"><b>Student Name:</b>{{$record->student_name}}</td>
            <td width="50%"><b>Clerkship Name:</b>{{$record->clerkship_name}}</td>
        </tr>
        <tr>
            <td width="50%"><b>Grad Year 2016:</b>{{$record->student_grand_year}}
                &nbsp;<b>Phone:</b>{{$record->student_phone_format}}
            </td>
            <td width="50%"><b>Clerkship Date:</b>{{$record->clerkship_date_to}}
                &nbsp;<b>to:</b>{{$record->clerkship_date_from}}
            </td>
        </tr>
        <tr>
            <td width="50%"><b>Email: </b>{{$record->student_email}}</td>
            <td width="50%">{{$record->clerkship_type}}</td>
        </tr>

        <tr>
            <td width="50%"><b>Current Address:</b>{{$record->student_curr_address}}</td>
            <td width="50%"><b>{{$record->clerkship_credit_type}}</b></td>
        </tr>

        <tr>
            <td width="50%"><b>City:</b>{{$record->student_city}}&nbsp;&nbsp;<b>State:</b>{{$record->student_state}}&nbsp;&nbsp;<b>Zip:</b>{{$record->student_zip}}
                &nbsp;</td>
            <td width="50%"><b>Printed Name/Degree of Clinical Trainer:</b><br>{{$record->clerkship_degree_trainer}}
            </td>
        </tr>

        <tr>
            <td width="50%"><b>PRN</b>{{$record->prn}}</td>
            <td width="50%"><b>Email of Clinical Trainer:</b> <br>{{$record->clerkship_clinical_email}}</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;background-color:#1D3365;"><span
                        style="font-size:20px; font-weight: bold;color:#fff; text-align: center;">Site Information</span>
            </td>

        </tr>
        <tr>
            <td width="50%"><b>Site Name:</b>{{$record->site_name}}</td>
            <td width="50%"><b>Contact Name</b>{{$record->site_contact_name}}</td>
        </tr>
        <tr>
            <td width="50%"><b>Address:</b>{{$record->site_address}}</td>
            <td width="50%"><b>Email:</b>{{$record->site_email}}</td>
        </tr>
        <tr>
            <td width="50%"><b>City:</b>{{$record->site_city}}&nbsp;<b>State:</b>{{$record->site_state}}
                &nbsp;<b>Zip:</b>{{$record->site_zip}}
            </td>
            <td width="50%"> <b>Phone:</b>{{$record->site_phone_format}}&nbsp;<b>Fax:</b>{{$record->site_fax_format}}
            </td>
        </tr>
        <tr>
            <td width="50%"><b>Address</b> to which application should be mailed if different than above</td>
            <td width="50%"><b>Name/Dept:{{$record->site_name_dept}}&nbsp;</b></td>
        </tr>
        <tr>

            <td width="50%"><b>City:</b>{{$record->site_diff_city}}&nbsp;&nbsp;<b>State:</b>{{$record->site_diff_state}}&nbsp;
                &nbsp;<b>Zip:</b>{{$record->site_diff_zip}}&nbsp;
            </td>
            <td width="50%">&nbsp;</td>
        </tr>

        <tr>
            <td colspan="2" style="text-align: center;background-color:#1D3365;"><span
                        style="font-size:20px;font-weight: bold; color:#fff; text-align: center;">UNECOM CLINICAL EDUCATION OFFICE</span>
            </td>

        </tr>
        <tr>
            <td width="50%"><b>This is clerkship is :</b>{{$record->unecom_clerkship}}&nbsp;</td>
            <td width="50%"><b># of Weeks :</b> {{$record->unecom_of_week}}</td>
        </tr>
        <tr>
            <td width="50%"><b>Date Received :</b>{{$record->unecom_dat_rec}}&nbsp;</td>
            <!--<b>Date Mailed : </b>-->{{$record->unecom_maild}}
            <td width="50%"><b>by :</b>{{$record->unecom_by_week}}&nbsp;</td>
        </tr>
    </table>
    <table border="1px" width="630px;">
        <tr>
            <td width="50%" style="text-align:center;background-color:#1D3365;"><span
                        style="text-align: center;font-weight: bold;font-size:20px; color:#fff;">Clerkship Confirmation and Information </span>
            </td>
            <td width="50%" style="text-align: center;background-color:#1D3365;"><span
                        style=" font-size:20px;font-weight: bold; color:#fff; text-align: center;">Evaluation Information </span>
            </td>
        </tr>

        <tr>
            <td width="50%">Upon your confirmation, this clerkship becomes an academic requirement to which the
                student is obligated.Only under extraordinary circumstances may a student be excused from this
                commitment.
            </td>
            <td width="50%">The evaluation of this medical student is an online document. Please provide the
                information below so we may send the evaluation link to appropriate person.
            </td>
        </tr>

        <tr>
            <td width="50%"><b>Clerkship Name :</b>{{$record->site_preceptor_conf_name}}&nbsp;</td>
            <td width="50%"><b>Name and Degree</b> of person responsible for evaluating this student’s performance:
            </td>
        </tr>

        <tr>
            <td width="50%"><b>Is the supervising physician Board certified or
                    Board eligible in this discipline?</b>{{$record->site_preceptor_conf_certified}}&nbsp;</td>
            <td width="50%"><b>Name/Degree:</b>{{$record->site_preceptor_eval_namdegree}}&nbsp;</td>
        </tr>

        <tr>
            <td width="50%"><b>Is this a Sub-Internship?</b>{{$record->site_preceptor_conf_internship}}&nbsp;</td>
            <td width="50%"><b>Email:</b>{{$record->site_preceptor_eval_email}}&nbsp;</td>
        </tr>

        <tr>
            <td width="50%"><b>Clerkship and dates if different from those requested :<br/>
                    clerkship Dates:</b>{{$record->site_preceptor_conf_from}}
                &nbsp;&nbsp;<b>to:</b>{{$record->site_preceptor_conf_to}}
            </td>
            <td width="50%"><b>Name/Email</b> of person, if other than physician, to whom evaluation link should be
                sent (e.g. office staff):
            </td>
        </tr>

        <tr>
            <td width="50%"><b>This clerkship is:</b>{{$record->site_preceptor_conf_clship}}&nbsp;</td>
            <td width="50%"><b>Name:</b>{{$record->site_preceptor_eval_name}}&nbsp;</td>
        </tr>

        <tr>
            <td width="50%"><b>by:</b>{{$record->site_preceptor_conf_by}}&nbsp;</td>
            <td width="50%"><b>Email:</b>{{$record->site_preceptor_eval_email_1}}&nbsp;</td>
        </tr>

        <tr>
            <td width="50%"><b>Email:</b>{{$record->site_preceptor_conf_email}}&nbsp;</td>
            <td width="50%">&nbsp;</td>
        </tr>

        <tr>
            <td width="50%"><b>Phone:</b>&nbsp;{{$record->site_preceptor_conf_phone}}
                <b>Date:</b>{{$record->site_preceptor_conf_dt}}
            </td>
            <td width="50%">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;background-color:#1D3365;"><span
                        style="font-size:18px;font-weight: bold; color:#fff; text-align: center;">Educational Agreement</span>
            </td>
        </tr>

        <tr>
            <td colspan="2" style="border: none"><span style="text-align:center;">This document will serve as the educational agreement for this clerkship . If a more detailed agreement is required, Please forward our agreement to, or request our agreement from, Jacqueline Hawkins at jhawkins1@une
                    .edu (Phone: 207-602-2527).</span></td>
        </tr>
    </table>
    <table border="1px" width="630px;">
        <tr>
            <td colspan="2" style="border: none">
                    <span style="float: left;font-size: 19px;padding: 11px 0;">Continued: {{$record->student_name}}</span>

                    <p><b>Host Site/Preceptor Rights and Responsibilities:</b></p>
                    <p>The HOST SITE/ Preceptor will offer an educational program in compliance
                        with mutually agreedupon standard and guidelines. The administration,
                        faculty and staff of HOST SITE/PRECEPTOR will conduct educational activities,
                        program development and student evaluation. Representatives of
                        the affiliates may meet in programmatic review periodically, as mutually agreeable.</p>
                    <p>The HOST SITE/PRECEPTOR, agrees to be responsible for developing and implementing educational
                        programs for students approved for this clerkship.The specific duties and responsibilities of
                        the students assign to HOST SITE/PRECEPTOR shall be in accordance with the clinical clerkship
                        training manual as maintained by UNECOM. </p>

                    <p>HOST SITE/PRECEPTOR will provide an orientation program for medical students during their first
                        clerkship to acquaint them with HOST SITE/PRECEPTOR protocols and facilities, and will provide
                        supervision of the students throughout training by members of the faculty of HOST SITE/PRECEPTOR
                        as well as any resident and intern staff. </p>
                    <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border: none">
                    <p><b>UNECOM RIGHTS AND Responsibilities: </b></p>
                    <p>UNECOM agrees to be responsible for the content of the clinical education syllabus related to
                        this clerkship,
                        In addition upon request, UNECOM will provide a clinical clerkship training manual as a
                        reference guide.
                        The above name student is formally enrolled in good standing in the predoctoral program at the
                        University of The New England, College Of Osteopathic Medicine (UNECOM).
                        As such she/he is covered, through UNECOM, by a Blanket professional liability insurance policy
                        at $2,000,000/$4,000,000 level.</p>
                    <p>Student at UNECOM are required to:
                        a) take and pass level 1 COMLEX-USA in order to begin third year clerkship;
                        b) maintain health Insurance coverage while enrolled at the University;
                        c) documents acceptable titers for measles, mumps, rubella and vericella;
                        d) document Hepatitis B series + titer(or Wavier);
                        e) documents annual tuberculosis screening;
                        f) documents current tetanus vaccine (TDAP or TD);
                        g) maintain certification of Basic Life Support And Advanced Cardiac Life Support;
                        h) annually update training regarding HIPPA regulations and OSHA bloodborne pathogens.</p>
                    <p style="text-align:right; padding-right:10px;margin: 0;">Guy A. DeFeo,</p>
                    <p style="text-align:right; padding-right:10px;margin: 0;">Associate Dean of Clinical Education</p>
                    <p style="text-align:right; padding-right:10px;margin: 0;">UNECOM</p>

            </td>

        </tr>
    </table>
    <table border="1px"  width="630px;">
                    <tr>
                        <td colspan="2"><b>Student:</b> List goals for this clerkship</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>1.</b>{{$record->student_goal_clerkship_1}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>2.</b>{{$record->student_goal_clerkship_2}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>3.</b>{{$record->student_goal_clerkship_3}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>Student:</b> Listed Clerkships you will have completed prior to this
                            requested clerkship
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>1.</b>{{$record->student_request_clerkship_1}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>2.</b>{{$record->student_request_clerkship_2}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>3.</b>{{$record->student_request_clerkship_3}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>4.</b>{{$record->student_request_clerkship_4}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>5.</b>{{$record->student_request_clerkship_5}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>6.</b>{{$record->student_request_clerkship_6}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>7.</b>{{$record->student_request_clerkship_7}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>8.</b>{{$record->student_request_clerkship_8}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>9.</b>{{$record->student_request_clerkship_9}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>10.</b>{{$record->student_request_clerkship_10}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>11.</b>{{$record->student_request_clerkship_11}}&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>12.</b>{{$record->student_request_clerkship_12}}&nbsp;</td>
                    </tr>
    </table>
    <table border="1px" width="630px;">
        <tr>
            <td colspan="2">
                <p>Submit This Registration A MINIMUM Of 45 Days In Advance Of The Intended Start Of Your Clerkship To
                    :</p>
                <div style="text-align:center;float:left;width: 100%; margin-bottom:10px;">
                    <div style="width:100%; float:left;  text-align:center;">
                        <span style=" float:left; text-align:center;"><b>Judy Beauchemin</b></span><br/>
                        <span style=" float:left;  text-align:center;"><b>jbeauchemin@une
                                .edu</b></span><br/>
                        <span style="float:left;  text-align:center;"><b>Tel: </b>207-602-2674</span><br/>
                    </div><br>
                    <div style="width:100%; float:right; text-align:center;">
                        <span style="float:left; text-align:center;"><b>Jacqueline Hawkins</b></span><br/>
                        <span style="float:left;text-align:center;"><b>jhawkins1@une.edu</b></span><br/>
                        <span style="float:left; text-align:center;"><b>Tel: </b>207-602-2527</span><br/>
                    </div>

                </div>
            </td>


        </tr>


    </table>
</div>
</body>
</html>