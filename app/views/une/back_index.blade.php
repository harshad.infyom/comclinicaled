@extends('layouts.master')

@section('content')

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/validationEngine.jquery.css') }}" type="text/css">
    <link href="{{ asset('assets/css/jquery.datepick.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php

    if (!empty($une_info['res'])) {
        echo '<section class="section01"><div class="container"><div class="row text-center"><p style="color:green;font-size:15px;">Successfully Submitted.<br>' . $une_info['download_msg'] . '</p></div></div></section>';
    }
    ?>
    <section class="section02">
        <form method="post" id="formID1" action="">
            <div class="container border-div">
                <div class="row">
                    <div class="col-md-12 title">Unecom Medical Student (Submit this Registration a MINIMUM of 45 days
                        in Advance….)
                    </div>
                    <div class="col-md-12 title-text"><b>Submit this Registration A MINIMUM of 45 Days In Advance Of The
                            Intended Start Of Your Clerkship</b><br/>
                        <p>Clinical Education MUST approve every Clerkship in advance of its start for you to be covered
                            by professional liability Insurance and receive clerkship credit</p></div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 bg-color">Student Information</div>
                            <div class="border-line" style="padding-top: 40px!important;">
                                <label>Student Name:</label><input type="text" id="std-name" placeholder="Student Name"
                                                                   name="student_name"
                                                                   class="input-email few-width validate[required] text-input"
                                                                   value="<?php echo Session::get('student_name'); ?>"/>
                            </div>
                            <div class="border-line frst" style="padding: 11.5px!important;">
                                <label>Grad Year: </label><select name="student_grand_year" class="input-box-c">
                                    <option value="">Grad Year <?php echo date('Y'); ?></option>
                                    <?php $x = 2030;
                                    $cuyear = date('Y');

                                    while($x >= $cuyear)
                                    {
                                    ?>
                                    <option value="<?php echo $cuyear; ?>"><?php echo $cuyear; ?></option>
                                    <?php
                                    $cuyear++;
                                    }

                                    ?>

                                </select>
                            </div>
                            <div class="border-line few-phone frst">
                                <label>Phone:</label>
                                <input type="text" placeholder="XXX" class="input-box-c" name="student_phone1"
                                       maxlength="3" style="width:21% ;"/>-<input type="text" placeholder="XXX"
                                                                                  class="input-box-c"
                                                                                  name="student_phone2" maxlength="3"
                                                                                  style="width:21% ;"/>-<input
                                        type="text" placeholder="XXXX" class="input-box-c" name="student_phone3"
                                        maxlength="4" style="width:20.5%;"/>
                            </div>
                            <div class="border-line frst">
                                <label>Email: </label><input type="text" placeholder="Email" class="input-email"
                                                             name="student_email"
                                                             value="<?php echo Session::get('email'); ?>"/>
                            </div>
                            <div class="border-line frst">
                                <label>Current Address:</label><input type="text" placeholder="Current Address"
                                                                      class="input-email validate[required] text-input"
                                                                      name="student_curr_address"/>
                            </div>
                            <div class="border-line frst">
                                <label>City:</label><input type="text" placeholder="City"
                                                           class="input-box-d validate[required] text-input"
                                                           name="student_city"/>
                            </div>
                            <div class="border-line frst">
                                <label>State:</label><input type="text" placeholder="State :"
                                                            class="input-box-d validate[required] text-input"
                                                            name="student_state"/>
                            </div>
                            <div class="border-line frst">
                                <label>Zip:</label><input type="text" placeholder="Zip :"
                                                          class="input-box-d validate[required] text-input"
                                                          name="student_zip"/>
                            </div>
                            <div class="border-line frst">
                                <label> PRN:</label> <input type="text" placeholder=""
                                                            class="input-box-d validate[required] text-input" name="prn"
                                                            value="132155321" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 bg-color">Clerkship Information</div>
                            <div class="border-line" style="padding-top: 40px!important;">
                                <label>Clerkship Name:</label><input type="text" placeholder="Clerkship Name"
                                                                     name="clerkship_name"
                                                                     class="input-email validate[required] text-input"/>
                            </div>
                            <div class="border-line few-phone frst">
                                <label>Clerkship Date:</label>
                                <input type="text" placeholder="mm/dd/yyyy" id="popupDatepicker"
                                       class="input-box-d validate[required] text-input" name="clerkship_date_to"/> to
                                <input type="text" placeholder="mm/dd/yyyy" id="popupDatepicker1"
                                       class="input-box-d validate[required] text-input datepicker"
                                       name="clerkship_date_from"/>
                            </div>

                            <div class="border-line scnd" style="">
                                <div type="text" class="clerkship-type">
                                    <div class="left-side">
                                        <span class="clerkship-title he-in"><label>Clerkship Type:</label></span></div>
                                    <div class="ryt-side">
							<span class="core-title">
								<input type="radio" class="core validate[required] radio" value="core"
                                       name="clerkship_type" checked/> Core
							</span>
							<span class="core-title">
								<input type="radio" value="selective" class="core validate[required] radio"
                                       name="clerkship_type"/> Selective
							</span>
							<span class="core-title">
								<input type="radio" value="elective" name="clerkship_type"
                                       class="core validate[required] radio"/> Elective
							</span>
							<span class="core-title">
								<input type="radio" value="non-credit" name="clerkship_type"
                                       class="core validate[required] radio"/> Non-Credit
							</span>
                                    </div>
                                </div>
                            </div>
                            <div class="border-line few-phone scnd" style="">
                                <div type="text" class="clerkship-type">
                                    <div class="left-side">
                                        <span class="clerkship-title"><label>Credit Type:</label></span></div>
                                    <div class="ryt-side">
							<span class="core-title">
								<input type="radio" value="aoa" class="core validate[required] radio"
                                       name="clerkship_credit_type" checked/> AOA
							</span>
							<span class="core-title">
								<input type="radio" class="core validate[required] radio" value="osteopathic physician"
                                       name="clerkship_credit_type"/> OP
								<span class="op"> (OSTEOPATHIC PHYSICIAN)</span>
							</span>
							<span class="core-title">
								<input type="radio" value="ama" class="core validate[required] radio"
                                       name="clerkship_credit_type"/> AMA
							</span>
                                    </div>
                                </div>
                            </div>
                            <div class="border-line few-h frst">
                                <div type="text" class="clerkship-type01">
                                    <span class="clerkship-title"><label>Printed Name/Degree of Clinical Trainer:</label></span>
                                    <input type="text" placeholder="Input Box" name="clerkship_degree_trainer"
                                           class="input-box validate[required] text-input"/>
                                </div>
                            </div>
                            <div class="border-line few-h frst">
                                <div type="text" class="clerkship-type01">
                                    <span class="clerkship-title"><label>Email of Clinical Trainer:</label></span>
                                    <input type="text" name="clerkship_clinical_email" placeholder="Input Box"
                                           class="input-box validate[required,custom[email]] text-input"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 bg-color">Site Information</div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="border-line">
                                <label>Site Name:</label><input type="text" placeholder="Site Name" name="site_name"
                                                                class="input-email validate[required] text-input"/>
                            </div>
                            <div class="border-line">
                                <label>Address:</label><input type="text" placeholder="Address" name="site_address"
                                                              class="input-email validate[required] text-input"/>
                            </div>
                            <div class="border-line">
                                <label>City:</label><input type="text" placeholder="City"
                                                           class="input-box-d validate[required] text-input"
                                                           name="site_city"/>
                            </div>
                            <div class="border-line">
                                <label>State:</label><input type="text" placeholder="State :"
                                                            class="input-box-d validate[required] text-input"
                                                            name="site_state"/>
                            </div>
                            <div class="border-line">
                                <label>Zip:</label><input type="text" placeholder="Zip :"
                                                          class="input-box-d validate[required] text-input"
                                                          name="site_zip"/>
                            </div>
                            <div class="border-line">
                                <div type="text" class="clerkship-type02">
                                    <span class="clerkship-title bold-line" style="color: #191717;font-size: 18px;">Address to which application should be mailed if different than above</span>
                                </div>
                            </div>
                            <div class="border-line">
                                <label>City:</label><input type="text" placeholder="City" class="input-box-d"
                                                           name="site_diff_city"/>
                            </div>
                            <div class="border-line">
                                <label>State:</label><input type="text" placeholder="State :" class="input-box-d"
                                                            name="site_diff_state"/>
                            </div>
                            <div class="border-line">
                                <label>Zip:</label><input type="text" placeholder="Zip :" class="input-box-d"
                                                          name="site_diff_zip"/>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="border-line">
                                <label>Contact Name:</label><input type="text" placeholder="Contact Name/Title"
                                                                   class="input-email validate[required] text-input"
                                                                   name="site_contact_name"/>
                            </div>
                            <div class="border-line">
                                <label>Email:</label><input type="text" placeholder="Email" name="site_email"
                                                            class="input-email"/>
                            </div>
                            <div class="border-line few-phone">
                                <label>Phone:</label><input type="text" placeholder="XXX" class="input-box-c"
                                                            name="site_phone" maxlength="3"
                                                            style="width:20.50% ;"/>-<input type="text"
                                                                                            placeholder="XXX"
                                                                                            class="input-box-c"
                                                                                            name="site_phone1"
                                                                                            maxlength="3"
                                                                                            style="width:21%;"/>-<input
                                        type="text" placeholder="XXXX" class="input-box-c" name="site_phone2"
                                        maxlength="4" style="width:21% ;"/>
                            </div>
                            <div class="border-line few-phone">
                                <label>Fax:</label><input type="text" placeholder="XXX" class="input-box-c"
                                                          name="site_fax" maxlength="3" style="width:21% ;"/>-<input
                                        type="text" placeholder="XXX" class="input-box-c" name="site_fax1" maxlength="3"
                                        style="width:21%;"/>-<input type="text" placeholder="XXX" class="input-box-c"
                                                                    name="site_fax2" maxlength="3"
                                                                    style="width:20.50%;"/>
                            </div>
                            <div class="border-line">
                                <label>Name/Dept:</label><input type="text" placeholder="Name/Dept"
                                                                class="input-email validate[required] text-input"
                                                                name="site_name_dept"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 bg-color">UNECOM CLINICAL EDUCATION OFFICE</div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="border-line frst" style="padding: 15px!important;">
                                <label>This is clerkship is ?:</label><input type="radio" name="unecom_clerkship"
                                                                             value="Approved" disabled/>&nbsp;Approved&nbsp;<input
                                        disabled type="radio" name="unecom_clerkship" value="Not Approved"/>&nbsp;Not
                                Approved
                            </div>
                            <div class="border-line frst">
                                <label>Date Received:</label>
                                <?php date_default_timezone_set('US/Eastern'); ?>
                                <input readonly type="text" id="uncomdate" class="input-box-d" name="unecom_dat_rec"
                                       value="<?php echo date("F j, Y, g:i a");?> "/>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="border-line scnd">
                                <label>&nbsp;# of Weeks:</label><input disabled type="text" name="unecom_of_week"
                                                                       class="weeks-input"/>&nbsp;By: &nbsp;<input
                                        disabled type="text" name="unecom_by_week" style="margin-left:35%;"
                                        class="weeks-input"/>
                            </div>
                        <!--<div class="border-line">
					<label>&nbsp;Date Mailed:</label> <input readonly type="text" placeholder="" id="uncomdate1" class="input-box-d text-input"   name="unecom_maild" value="<?php //echo date("F j, Y, g:i a");?>" /> 
					</div>-->
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 bg-color">HOST SITE/PRECEPTOR</div>
                    <div class="col-md-12 text-center">
                        <div class="row text-center">
                            <!--Please complete the sections below -->
                            <!--and fax-return only the first page of this document to the UNECOM Office of Clinical Education at 207.602.5908-->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="border-line bg-color">
                                Clerkship Confirmation and Information
                            </div>
                            <div class="border-line scnd">
                                Upon your confirmation, this clerkship becomes an academic requirement to which the
                                student is obligated. Only under extraordinary circumstances may a student be excused
                                from this commitment.
                            </div>
                            <div class="border-line frst">
                                <label>Clerkship Name :</label>
                                <input type="text" width="" disabled name="site_preceptor_conf_name"
                                       class=" text-input"/>
                            </div>
                            <div class="border-line few-heigh scnd"
                                 style="padding: 25px 10px!IMPORTANT;overflow: hidden!IMPORTANT;height: 114px;">
                                <label><b>Is the supervising physician Board certified or Board eligible in this
                                        discipline?</b></label><input type="radio" disabled
                                                                      name="site_preceptor_conf_certified"
                                                                      class=" radio" value="Yes"/>&nbsp;Yes &nbsp;<input
                                        type="radio" disabled name="site_preceptor_conf_certified"
                                        class="validate[required] radio" value="No"/>&nbsp;No
                            </div>
                            <div class="border-line frst" style="padding: 17.5px 10px!important">
                                <label><b>Is this a Sub-Internship?</b></label><input type="radio"
                                                                                      name="site_preceptor_conf_internship"
                                                                                      class=" radio" value="Yes"
                                                                                      disabled/>&nbsp;Yes &nbsp;<input
                                        type="radio" name="site_preceptor_conf_internship" class=" radio" disabled
                                        value="No"/>&nbsp;No
                            </div>
                            <div class="border-line few-heigh scnd" style="padding: 12px 10px!important">
                                <label>Clerkship dates if different from those Requested:</label>
                                <label>start date:</label><input type="text" placeholder="mm/dd/yyyy"
                                                                 id="site_preceptor" disabled
                                                                 class="input-box-d  text-input"
                                                                 name="site_preceptor_conf_from"/>
                                <label style="margin-left:195px;">end date:</label><input type="text" disabled
                                                                                          placeholder="mm/dd/yyyy"
                                                                                          id="site_preceptor1"
                                                                                          class="input-box-d  text-input"
                                                                                          name="site_preceptor_conf_to"
                                                                                          style="margin-left:195px;"/>
                            </div>
                            <div class="border-line">
                                <label><b>This clerkship is </label><input type="radio"
                                                                           name="site_preceptor_conf_clship" class=""
                                                                           disabled value="Approved"/>&nbsp;Approved
                                &nbsp;<input type="radio" name="site_preceptor_conf_clship" class=""
                                             value="NOT Approved" disabled/>&nbsp;NOT Approved</b>
                            </div>
                            <div class="border-line">
                                <label>by </label><input type="text" class="input-box-d  text-input" style="" disabled
                                                         name="site_preceptor_conf_by"/>
                            </div>

                            <div class="border-line">
                                <label>Email</label><input type="text" class="input-box-d  text-input" disabled style=""
                                                           name="site_preceptor_conf_email"/>
                            </div>
                            <div class="border-line few-phone">
                                <label>Phone</label><input type="text" disabled class="input-box-d  text-input"
                                                           name="site_preceptor_conf_phone" style="width:20%"
                                                           maxlength="3" placeholder="XXX"/>
                                -
                                <input type="text" disabled class="input-box-d  text-input"
                                       name="site_preceptor_conf_phone1" style="width:20%" maxlength="3"
                                       placeholder="XXX"/>
                                -
                                <input type="text" disabled style="width:20%" class="input-box-d  text-input"
                                       name="site_preceptor_conf_phone2" maxlength="4" placeholder="XXXX"/>
                            </div>
                            <div class="border-line">
                                <label>Date </label><input type="text" disabled placeholder="mm/dd/yyyy"
                                                           id="site_preceptor_info_date" class="input-box-d"
                                                           name="site_preceptor_conf_dt"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="border-line bg-color">
                                Evaluation Information
                            </div>
                            <div class="border-line scnd" style="padding-bottom: 30px!important;">
                                The evaluation of this medical student is an online Document. Please provide the
                                information below so we may send the evaluation link to appropriate person.
                            </div>
                            <div class="border-line frst"
                                 style="height: 57px!important; font-size:15px; font-weight:bold;">
                                Name and Degree of person responsible for evaluating this student’s performance :
                            </div>
                            <div class="border-line frst">
                                <label>Name/Degree</label><input type="text" disabled style=""
                                                                 class="input-box-d  text-input"
                                                                 name="site_preceptor_eval_namdegree"/>
                            </div>
                            <div class="border-line frst">
                                <label>Email</label><input type="text" disabled class="input-box-d  text-input" style=""
                                                           name="site_preceptor_eval_email"/>
                            </div>

                            <div class="border-line frst">
                                Name/Email of person, if other than physician, to whom evaluation link should be sent
                                (e.g. office staff):
                            </div>

                            <div class="border-line frst">
                                <label>Name</label><input type="text" disabled class="input-box-d" style=""
                                                          name="site_preceptor_eval_name"/>
                            </div>
                            <div class="border-line frst">
                                <label>Email</label><input type="text" disabled style="" class="input-box-d"
                                                           name="site_preceptor_eval_email_1"/>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row" style="border-bottom:2px solid #CCCCCC;">
                    <div class="col-md-12 bg-color">Your Agreement</div>

                    <div class="row">
                        <div class="col-md-12" style="padding-left: 25px;">
                            This Document will serve as the Educational agreement for this Clerkship. If a more detailed
                            agreement is required, Please forward Our agreement to, or request your agreement from,
                            Jacqueline Hawkins at jhawkins1@une.edu (Phone: 207-602-2527).
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 student-name"><span
                                id="name-std"></span><?php //echo Session::get('student_name'); ?></div>
                    <div class="col-md-12 student-text">
                        <span class="host-title">Host Site/Preceptor Rights and Responsibilities:</span><br>
                        <p>The HOST SITE/ Preceptor will offer an Educational Program in Compliance with Mutually Agreed
                            upon Standards and Guidelines. The Administration, Faculty and staff of HOST SITE/PRECEPTOR
                            will conduct Educational Activities, Program Development and Student Evaluation.
                            Representatives of the Affiliates may meet in Programmatic Review Periodically, as Mutually
                            Agreeable.</p>
                        <p>The HOST SITE/PRECEPTOR Agrees to be Responsible for Developing and Implementing Educational
                            Programs For Students Approved for this Clerkship. The Specific Duties and Responsibilities
                            of the Students Assigned to HOST SITE/PRECEPTOR shall be in Accordance with the Clinical
                            Clerkship Training manual as Maintained by UNECOM.</p>
                        <p>HOST SITE/PRECEPTOR will Provide an Orientation Program for Medical Students during their
                            first Clerkship to Acquaint them with HOST SITE/PRECEPTOR Protocols and Facilities and will
                            Provide Supervision of the Students Throughout Training by Members of the Faculty of HOST
                            SITE/PRECEPTOR, as well as any Resident and intern staff.</p>

                    </div>
                    <div class="col-md-12 student-text">
                        <span class="host-title">UNECOM RIGHTS AND Responsibilities: </span><br>
                        <p>UNECOM Agrees to be Responsible for the Content of the Clinical Education Syllabus Related to
                            this Clerkship.
                            In Addition upon Request, UNECOM will Provide a Clinical Clerkship Training manual as a
                            Reference Guide.
                            The Above Named Student is Formally Enrolled in Good Standing in the Pre-doctoral Program at
                            the University of The New England, COLLEGE OF OSTEOPATHIC MEDICINE (UNECOM).
                            As such she/he is Covered, through UNECOM, by a Blanket Professional Liability Insurance
                            Policy at $2,000,000/$4,000,000 level.</p>
                        <p>

                            a) Students at UNECOM are Required to a take and pass level 1 COMLEX-USA in order to Begin
                            third year clerkship is Maintain Health Insurance Coverage while Enrolled at the University;
                            b) Document Acceptable Titers for Measles, Mumps, Rubella and Varicella;
                            c) Document Hepatitis B Series + titer(or Waiver);
                            d) Document Annual Tuberculosis Screening;
                            e) Document Current Tetanus Vaccine (TDAP or TD);
                            f) Maintain Certification of Basic Life Support And Advanced Cardiac Life Support;
                            g) Annually Update Training Regarding HIPPA Regulations And OSHA Blood Borne Pathogens.

                        </p>


                    </div>
                    <div class="col-md-11 student-text text-right">
                        Guy A. DeFeo,<br>
                        Associate Dean of Clinical Education<br>
                        UNECOM
                    </div>
                    <div class="col-md-12 student-text">
                        <span class="table-title">Student: <span
                                    class="table-title01">List goals for this clerkship</span> </span>
                        <table width="100%" border="1" class="table-style">
                            <tr>
                                <td>1.<input type="text" name="student_goal_clerkship_1"/></td>
                            </tr>
                            <tr>
                                <td>2.<input type="text" name="student_goal_clerkship_2"/></td>
                            </tr>
                            <tr>
                                <td>3.<input type="text" name="student_goal_clerkship_3"/></td>
                            </tr>
                        </table>
                    </div>
                    <div class="row student-text">
                        <div class="col-md-12">
                            <span class="table-title">Student: <span class="table-title01">List Clerkships you will have completed prior to this requested Clerkships</span> </span>
                            <style>
                                .row.student-text input[type="text"].marg-left-11 {
                                    margin-left: 11px !important;
                                }
                            </style>
                            <table width="100%" class="table-style" border="1" class="table-style">
                                <tr>
                                    <td>1<input class="marg-left-11" type="text" name="student_request_clerkship_1"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2<input class="marg-left-11" type="text" name="student_request_clerkship_2"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3<input class="marg-left-11" type="text" name="student_request_clerkship_3"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4<input class="marg-left-11" type="text" name="student_request_clerkship_4"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5<input class="marg-left-11" type="text" name="student_request_clerkship_5"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6<input class="marg-left-11" type="text" name="student_request_clerkship_6"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7<input class="marg-left-11" type="text" name="student_request_clerkship_7"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8<input class="marg-left-11" type="text" name="student_request_clerkship_8"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9<input class="marg-left-11" type="text" name="student_request_clerkship_9"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10<input type="text" name="student_request_clerkship_10"/></td>
                                </tr>
                                <tr>
                                    <td>11<input type="text" name="student_request_clerkship_11"/></td>
                                </tr>
                                <tr>
                                    <td>12<input type="text" name="student_request_clerkship_12"/></td>
                                </tr>
                            </table>
                        </div>
                        <!-- <div class="col-md-3 photo-color">Photo</div> -->
                    </div>

                    <div class="col-md-12 student-text">
                        Submit This Registration A MINIMUM Of 45 Days In Advance Of The Intended Start Of Your Clerkship
                        To :
                    </div>
                    <div class="row student-text text-center">
                        <div class="col-md-6">
                            <b>Judy Beauchemin<br>
                                jbeauchemin@une.edu<br>
                                Tel 207-602-2674</b>
                        </div>
                        <div class="col-md-6 border-text">
                            <b>Jacqueline Hawkins<br>
                                jhawkins1@une.edu<br>
                                Tel 207-602-2527</b>
                        </div>
                    </div>
                    <div class="row student-text text-center subbtn">
                        <input type="submit" name="subform" value="Submit/Print"/>

                        <!--<input type="submit" name="Print" value="Print"  onClick="window.print()"/>-->
                    </div>
                </div>
            </div>
        </form>
    </section>


    <!-- jQuery -->
    <script src="{{ asset('assets/js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.validationEngine-en.js') }}" type="text/javascript"
            charset="utf-8"></script>
    <script src="{{ asset('assets/js/jquery.validationEngine.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('assets/js/jquery.plugin.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.datepick.js') }}"></script>
    <script>

        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID1").validationEngine();

            jQuery(function () {
                //jQuery('#popupDatepicker').datepick();
                //var date45 = '<?php //echo date("D M j G:i:s T Y", strtotime("-45 days")); ?>';
                //console.log(date45);
                var toDate;
                jQuery('#popupDatepicker').datepick({
                    onSelect: function (date) {
                        //console.log(date);
                        toDate = date[0];
                        //console.log(toDate);
                    }
                });
                jQuery('#popupDatepicker1').datepick({
                    //  minDate: new Date(<?php //echo date("Y", $time1)?>, <?php //echo date("m", $time1)?> - 1, <?php //echo date("d", $time1)?>)

                    onSelect: function (dates) {
                        //alert('The chosen date(s): ' + dates);
                        var selectDate = dates[0];
                        //console.log(selectDate);
                        //console.log(date45);

                        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                        var firstDate = new Date(toDate);
                        var secondDate = new Date(selectDate);

                        var diffDays = Math.round((secondDate.getTime() - firstDate.getTime()) / (oneDay));
                        console.log(diffDays);
                        if (diffDays > 45) {
                            jQuery("#warning").remove();
                            jQuery('#popupDatepicker').parent().append('<p id="warning">Pls note this is before the 45 day request date</p>');
                        } else {
                            jQuery("#warning").remove();
                        }
                    },

                });

                jQuery('#popupDatepicker1').datepick();
                //jQuery('#uncomdate').datepick();
                //jQuery('#uncomdate1').datepick();
                jQuery('#site_preceptor').datepick();
                jQuery('#site_preceptor1').datepick();
                jQuery('#site_preceptor_info_date').datepick();

            });
            jQuery("#std-name").on("blur", function () {
                jQuery("#name-std").html($(this).val());
            });

        });
    </script>
    <style>
        #popupDatepicker {
            color: red !important;
        }

        #warning {
            color: red !important;
            font-size: 12px;
        }

        }
        /*@media screen and (min-width: 1400px) {*/
        div.frst {
            min-height: 80px;
        }

        div.scnd {
            min-height: 160px;
        }

        /*}*/
    </style>

    @endsection