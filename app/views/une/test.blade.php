<?php

$htm ='<html>
<head>
</head>
<style>
td{width:350px;}

</style>
<body >


<p><b>Host Site/Preceptor Rights and Responsibilities:</b></p>
<p>The HOST SITE/ Preceptor will offer an educational program in compliance with mutually agreed upon standard and guidelines. The administration, faculty and staff of HOST SITE/PRECEPTOR will conduct educational activities, program development and student evaluation. Representatives of the affiliates may meet in programmatic review periodically, as mutually agreeable.</p>
<p>The HOST SITE/PRECEPTOR, agrees to be responsible for developing and implementing educational programs for students approved for this clerkship.The specific duties and responsibilities of the students assign to HOST SITE/PRECEPTOR shall be in accordance with the clinical clerkship training manual as maintained by UNECOM. </p>

<p>HOST SITE/PRECEPTOR will provide an orientation program for medical students during their first clerkship to acquaint them with HOST SITE/PRECEPTOR protocols and facilities, and will provide supervision of the students throughout training by members of the faculty of HOST SITE/PRECEPTOR as well as any resident and intern staff. </p>
<p>&nbsp;</p>
<p><b>UNECOM RIGHTS AND Responsibilities: </b></p>
<p>UNECOM agrees to be responsible for the content of the clinical education syllabus related to this clerkship,
 In addition upon request, UNECOM will provide a clinical clerkship training manual as a reference guide.
 The above name student is formally enrolled in good standing in the predoctoral program at the University of The New England, College Of Osteopathic Medicine (UNECOM).
 As such she/he is covered, through UNECOM, by a Blanket professional liability insurance policy at $2,000,000/$4,000,000 level.</p>
<p>Student at UNECOM are required to:
  a) take and pass level 1 COMLEX-USA in order to begin third year clerkship;
  b) maintain health Insurance coverage while enrolled at the University;
  c) documents acceptable titers for measles, mumps, rubella and vericella;
  d) document Hepatitis B series + titer(or Wavier);
  e) documents annual tuberculosis screening;
  f) documents current tetanus vaccine (TDAP or TD);
  g) maintain certification of Basic Life Support And Advanced Cardiac Life Support;
  h) annually update training regarding HIPPA regulations and OSHA bloodborne pathogens.</p>
<p style="text-align:right; padding-right:10px;margin: 0;">Guy A. DeFeo,</p>
<p style="text-align:right; padding-right:10px;margin: 0;">Associate Dean of Clinical Education</p>
<p style="text-align:right; padding-right:10px;margin: 0;">UNECOM</p>


	<td width="230px;">For Image:</td>
</tr>

<tr>

<td colspan="2" width="630px;" >
<p>&nbsp;</p>
<p>Submit This Registration A MINIMUM Of 45 Days In Advance Of The Intended Start Of Your Clerkship To :</p>
<p>&nbsp;</p>
<div style="width:630px; float:left; margin-bottom:10px;">

<div style="width:315px; float:left; border-right:2px solid #000; text-align:center;">
<span style="width:315px; float:left; text-align:center;"><b>Judy Beauchemin</b></span><br />
<span style="width:315px; float:left;  text-align:center;"><b>jbeauchemin@une.edu</b></span><br />
<span style="width:315px; float:left;  text-align:center;"><b>Tel: </b>207-602-2674</span><br />
</div>
<div style="width:315px; float:left; text-align:center;">
<span style="width:315px; float:left; text-align:center;"><b>Jacqueline Hawkins</b></span><br />
<span style="width:315px; float:left;text-align:center;"><b>jhawkins1@une.edu</b></span><br />
<span style="width:315px; float:left; text-align:center;"><b>Tel: </b>207-602-2527</span><br />
</div>

</div>
</td>


</tr>

</table>

</div>

</body>

</html>';




//============================================================+
// END OF FILE
//============================================================+

