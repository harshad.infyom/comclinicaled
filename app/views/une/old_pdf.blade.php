<?php

//============================================================+
// File name   : PDF
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once( app_path('views').'/une/tcpdf_include.php');
require_once( app_path('Library').'/tcpdf\tcpdf.php');
//require_once('../une/tcpdf_include.php');

// create new PDF document
$your_width= '1140px';
$your_height = '';
$custom_layout = array($your_width, $your_height);
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,  $custom_layout, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('');
$pdf->SetTitle('');
$pdf->SetSubject('');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//print_r($une_info); die;

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$student_name=$une_info['stud_info']['student_name'];
$student_grand_year=$une_info['stud_info']['student_grand_year'];
$student_phone=$une_info['stud_info']['student_phone'];
$prn=$une_info['stud_info']['prn'];
$student_email=$une_info['stud_info']['student_email'];
$student_curr_address=$une_info['stud_info']['student_curr_address'];
$student_city=$une_info['stud_info']['student_city'];
$student_state=$une_info['stud_info']['student_state'];
$student_zip=$une_info['stud_info']['student_zip'];
$clerkship_name=$une_info['stud_info']['clerkship_name'];
$clerkship_date_to=$une_info['stud_info']['clerkship_date_to'];
$clerkship_date_from=$une_info['stud_info']['clerkship_date_from'];
$clerkship_type=$une_info['stud_info']['clerkship_type'];
$clerkship_credit_type=$une_info['stud_info']['clerkship_credit_type'];
$clerkship_degree_trainer=$une_info['stud_info']['clerkship_degree_trainer'];
$clerkship_clinical_email=$une_info['stud_info']['clerkship_clinical_email'];
$site_name=$une_info['stud_info']['site_name'];
$site_address=$une_info['stud_info']['site_address'];
$site_city=$une_info['stud_info']['site_city'];
$site_state=$une_info['stud_info']['site_state'];
$site_zip=$une_info['stud_info']['site_zip'];
$site_diff_city=$une_info['stud_info']['site_diff_city'];
$site_diff_state=$une_info['stud_info']['site_diff_state'];
$site_diff_zip=$une_info['stud_info']['site_diff_zip'];
$site_contact_name=$une_info['stud_info']['site_contact_name'];
$site_email=$une_info['stud_info']['site_email'];
$site_phone=$une_info['stud_info']['site_phone'];
$site_fax=$une_info['stud_info']['site_fax'];
$site_name_dept=$une_info['stud_info']['site_name_dept'];
$unecom_clerkship=$une_info['stud_info']['unecom_clerkship'];
$unecom_dat_rec=$une_info['stud_info']['unecom_dat_rec'];
$unecom_of_week=$une_info['stud_info']['unecom_of_week'];
$unecom_by_week=$une_info['stud_info']['unecom_by_week'];
$unecom_maild=$une_info['stud_info']['unecom_maild'];

//var_dump($unecom_maild); die;

$site_preceptor_conf_name=$une_info['stud_info']['site_preceptor_conf_name'];
$site_preceptor_conf_certified=$une_info['stud_info']['site_preceptor_conf_certified'];
$site_preceptor_conf_internship=$une_info['stud_info']['site_preceptor_conf_internship'];
$site_preceptor_conf_from=$une_info['stud_info']['site_preceptor_conf_from'];
$site_preceptor_conf_to=$une_info['stud_info']['site_preceptor_conf_to'];
$site_preceptor_conf_clship=$une_info['stud_info']['site_preceptor_conf_clship'];
$site_preceptor_conf_by=$une_info['stud_info']['site_preceptor_conf_by'];
$site_preceptor_conf_email=$une_info['stud_info']['site_preceptor_conf_email'];
$site_preceptor_conf_phone=$une_info['stud_info']['site_preceptor_conf_phone'];
$site_preceptor_conf_dt=$une_info['stud_info']['site_preceptor_conf_dt'];
$site_preceptor_eval_namdegree=$une_info['stud_info']['site_preceptor_eval_namdegree'];
$site_preceptor_eval_email=$une_info['stud_info']['site_preceptor_eval_email'];
$site_preceptor_eval_name=$une_info['stud_info']['site_preceptor_eval_name'];
$site_preceptor_eval_email_1=$une_info['stud_info']['site_preceptor_eval_email_1'];
$student_goal_clerkship_1=$une_info['stud_info']['student_goal_clerkship_1'];
$student_goal_clerkship_2=$une_info['stud_info']['student_goal_clerkship_2'];
$student_goal_clerkship_3=$une_info['stud_info']['student_goal_clerkship_3'];
$student_request_clerkship_1=$une_info['stud_info']['student_request_clerkship_1'];
$student_request_clerkship_2=$une_info['stud_info']['student_request_clerkship_2'];
$student_request_clerkship_3=$une_info['stud_info']['student_request_clerkship_3'];
$student_request_clerkship_4=$une_info['stud_info']['student_request_clerkship_4'];
$student_request_clerkship_5=$une_info['stud_info']['student_request_clerkship_5'];
$student_request_clerkship_6=$une_info['stud_info']['student_request_clerkship_6'];
$student_request_clerkship_7=$une_info['stud_info']['student_request_clerkship_7'];
$student_request_clerkship_8=$une_info['stud_info']['student_request_clerkship_8'];
$student_request_clerkship_9=$une_info['stud_info']['student_request_clerkship_9'];
$student_request_clerkship_10=$une_info['stud_info']['student_request_clerkship_10'];
$student_request_clerkship_11=$une_info['stud_info']['student_request_clerkship_11'];
$student_request_clerkship_12=$une_info['stud_info']['student_request_clerkship_12'];


/*PHONE AND FAX FORMAT*/

	$student_phone_format = substr($student_phone, 0, 3)."-".substr($student_phone, 3, 3)."-".substr($student_phone,6);
	$site_fax_format = substr($site_fax, 0, 3)."-".substr($site_fax, 3, 3)."-".substr($site_fax,6);
	$site_phone_format = substr($site_phone, 0, 3)."-".substr($site_phone, 3, 3)."-".substr($site_phone,6);

$htm ='<html>
<head>
</head>
<style>
td{width:350px;}

</style>
<body >
<div style="width:1140px; margin:0px auto;">
<table width="840px">
<tr>
<td colspan="2" width="700px;" style="text-align:center;"><img src="http://nerdthemes.co/laravel/assets/img/logo.png"></td>
</tr>
<br><br>
<tr><td colspan="2" width="700px;" ><p style="width:700px;text-align:center;text-transform: uppercase; font-size:18px; margin-bottom: 15px;">Clerkship Registration and Approval From (CRA)</p></td></tr>

</table>

<table border="2px" width="630px;">
<tr><td colspan="2" width="630px;" style="text-align=center; background-color:#1D3365; color:#fff; padding:4px;"><p style="color: #fff;   font-size: 12px;text-align: center; padding:9px;text-transform: uppercase;">Unecom Medical Student (Submit this Registration a MINIMUM of 45 days in Advance...)</p></td></tr>
<tr><td colspan="2"  width="630px;" ><p style=" color: #000;font-size: 11px;padding: 4px 2px;text-align: center;"><b>Submit this Registration A MINIMUM of 45 Days In Advance Of The Intended Start Of Your Clerkship</b><br />Clinical Education MUST approve every clerkship in advance of its start for you to be covered by professional liability insurance and receive clerkship credit</p></td></tr>
<tr>
<td width="315px;" style="background-color:#1D3365;"><span style="padding:4px; font-size:20px; color:#fff; text-align: center;">Student Information</span></td>
<td width="315px;" style="background-color:#1D3365;"><span style=" padding:4px;font-size:20px; color:#fff; text-align: center;">Clerkship Information</span></td>
</tr>
<tr>
<td width="315px;"><b>Student Name:</b>'.$student_name.'</td>
<td width="315px;"><b>Clerkship Name:</b>'.$clerkship_name.'</td>
</tr>

<tr>
<td width="315px;"><b>Grad Year 2016:</b>'.$student_grand_year.' &nbsp;<b>Phone:</b>'.$student_phone_format.'</td>
<td width="315px;"><b>Clerkship Date:</b>'.$clerkship_date_to.' &nbsp;<b>to:</b>'.$clerkship_date_from.'</td>
</tr>

<tr>
<td width="315px;"><b>Email: </b>'.$student_email.'</td>
<td width="315px;">'.$clerkship_type.'</td>
</tr>

<tr>
<td width="315px;"><b>Current Address:</b>'.$student_curr_address.' </td>
<td width="315px;"><b>'.$clerkship_credit_type.'</b></td>
</tr>

<tr>
<td width="315px;"><b>City:</b>'.$student_city.' &nbsp;<b>State:</b>'.$student_state.'&nbsp;<b>Zip:</b>'.$student_zip.' &nbsp;</td>
<td width="315px;"><b>Printed Name/Degree of Clinical Trainer:</b><br>'.$clerkship_degree_trainer.'</td>
</tr>

<tr>
<td width="315px;"><b>PRN</b>'.$prn.'</td>
<td width="315px;"><b>Email of Clinical Trainer:</b> <br>'.$clerkship_clinical_email.'</td>
</tr>
<tr>
<td width="315px;"  style="background-color:#1D3365;"><span style="padding:4px; font-size:20px; color:#fff; text-align: center;">Site Information</span></td>

</tr>
<tr>
<td width="315px;"><b>Site Name:</b>'.$site_name.'</td>
<td width="315px;"><b>Contact Name</b>'.$site_contact_name.'</td>
</tr>
<tr>
<td width="315px;"><b>Address:</b>'.$site_address.'</td>
<td width="315px;"><b>Email:</b>'.$site_email.'</td>
</tr>
<tr>
<td width="315px;"><b>City:</b>'.$site_city.'&nbsp;<b>State:</b>'.$site_state.'&nbsp;<b>Zip:</b>'.$site_zip.'</td>
<td width="315px;"><b>Phone:</b>'.$site_phone_format.'&nbsp;<b>Fax:</b>'.$site_fax_format.'</td>
</tr>
<tr>
<td width="315px;"><b>Address</b> to which application should be mailed if different than above</td>
<td width="315px;"><b>Name/Dept:'.$site_name_dept.'&nbsp;</b></td>
</tr>
<tr>

<td width="315px;"><b>City:</b>'.$site_diff_city.'&nbsp;<b>State:</b>'.$site_diff_state.'&nbsp;<b>Zip:</b>'.$site_diff_zip.'&nbsp;</td>
<td width="315px;">&nbsp;</td>
</tr>

<tr>
<td colspan="2"  width="630px;"  style="background-color:#1D3365;"><span style="padding:4px; font-size:20px; color:#fff; text-align: center;">UNECOM CLINICAL EDUCATION OFFICE</span></td>

</tr>
<tr>
<td width="315px;"><b>This is clerkship is :</b>'.$unecom_clerkship.'&nbsp;</td>
<td width="315px;"><b># of Weeks :</b> '.$unecom_of_week.'</td>
</tr>
<tr>
<td width="315px;"><b>Date Received :</b>'.$unecom_dat_rec.'&nbsp;</td>
<!--<b>Date Mailed : </b>-->'.$unecom_maild.'
<td width="315px;"><b>by :</b>'.$unecom_by_week.'&nbsp;</td>
</tr>
</table>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<table border="2px">
<tr>
<td colspan="2" width="630px;"  style="background-color:#1D3365;"><span style=" padding:4px; font-size:20px; color:#fff; text-align: center;">HOST SITE/PRECEPTOR</span></td>

</tr>
<!-- <tr>
<td colspan="2" width="630px;"><p style="padding:4px;  color:#000; text-align: center;">Please complete the sections below </p></td>

</tr> -->

<tr>
<td width="315px;"  style="background-color:#1D3365;"><span style="padding:4px; font-size:20px; color:#fff; text-align: center;">Clerkship Confirmation and Information </span></td>
<td width="315px;"  style="background-color:#1D3365;"><span style="background-color:#1D3365; padding:4px; font-size:20px; color:#fff; text-align: center;">Evaluation Information </span></td>
</tr>

<tr>
<td width="315px;">Upon your confirmation, this clerkship becomes an academic requirement to which the student is obligated.Only under extraordinary circumstances may a student be excused from this commitment. </td>
<td width="315px;">The evaluation of this medical student is an online document. Please provide the information below so we may send the evaluation link to appropriate person. </td>
</tr>

<tr>
<td width="315px;"><b>Clerkship Name :</b>'.$site_preceptor_conf_name.'&nbsp;</td>
<td width="315px;"><b>Name and Degree</b> of person responsible for evaluating this student’s performance: </td>
</tr>

<tr>
<td width="315px;"><b>Is the supervising physician Board certified or
Board eligible in this discipline?</b>'.$site_preceptor_conf_certified.'&nbsp;</td>
<td width="315px;"><b>Name/Degree:</b>'.$site_preceptor_eval_namdegree.'&nbsp;</td>
</tr>

<tr>
<td width="315px;"><b>Is this a Sub-Internship?</b>'.$site_preceptor_conf_internship.'&nbsp;</td>
<td width="315px;"><b>Email:</b>'.$site_preceptor_eval_email.'&nbsp;</td>
</tr>

<tr>
<td width="315px;"><b>Clerkship and dates if different from those requested :<br />
clerkship Dates:</b>'.$site_preceptor_conf_from.'&nbsp;&nbsp;<b>to:</b>'.$site_preceptor_conf_to.'</td>
<td width="315px;"><b>Name/Email</b> of person, if other than physician, to whom evaluation link should be sent (e.g. office staff): </td>
</tr>

<tr>
<td width="315px;"><b>This clerkship is:</b>'.$site_preceptor_conf_clship.'&nbsp;</td>
<td width="315px;"><b>Name:</b>'.$site_preceptor_eval_name.'&nbsp;</td>
</tr>

<tr>
<td width="315px;"><b>by:</b>'.$site_preceptor_conf_by.'&nbsp;</td>
<td width="315px;"><b>Email:</b>'.$site_preceptor_eval_email_1.'&nbsp;</td>
</tr>

<tr>
<td width="315px;"><b>Email:</b>'.$site_preceptor_conf_email.'&nbsp;</td>
<td  width="315px;">&nbsp;</td>
</tr>

<tr>
<td width="315px;"><b>Phone:</b>&nbsp;'.$site_preceptor_conf_phone.'<b>Date:</b>'.$site_preceptor_conf_dt.'</td>
<td  width="315px;">&nbsp;</td>
</tr>
<tr>
<td colspan="2" width="630px;"  style="background-color:#1D3365;"><span style="background-color:#1D3365; padding:4px; font-size:20px; color:#fff; text-align: center;">Educational Agreement</span></td>
</tr>

<tr><td width="630px;"><span style="text-align:center;">This document will serve as the educational agreement for this clerkship . If a more detailed agreement is required, Please forward our agreement to, or request our agreement from, Jacqueline Hawkins at jhawkins1@une.edu (Phone: 207-602-2527).</span> </td></tr>
</table>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<table border="2px">

<tr>
<td colspan="2" width="630px;" >
<div style="padding:10px; padding-left:3px;">
<span style="float: left;font-size: 19px;padding: 11px 0;width: 840px;">Continued: '.$student_name.'</span>

<p><b>Host Site/Preceptor Rights and Responsibilities:</b></p>
<p>The HOST SITE/ Preceptor will offer an educational program in compliance with mutually agreed upon standard and guidelines. The administration, faculty and staff of HOST SITE/PRECEPTOR will conduct educational activities, program development and student evaluation. Representatives of the affiliates may meet in programmatic review periodically, as mutually agreeable.</p>
<p>The HOST SITE/PRECEPTOR, agrees to be responsible for developing and implementing educational programs for students approved for this clerkship.The specific duties and responsibilities of the students assign to HOST SITE/PRECEPTOR shall be in accordance with the clinical clerkship training manual as maintained by UNECOM. </p>

<p>HOST SITE/PRECEPTOR will provide an orientation program for medical students during their first clerkship to acquaint them with HOST SITE/PRECEPTOR protocols and facilities, and will provide supervision of the students throughout training by members of the faculty of HOST SITE/PRECEPTOR as well as any resident and intern staff. </p>
<p>&nbsp;</p>
<p><b>UNECOM RIGHTS AND Responsibilities: </b></p>
<p>UNECOM agrees to be responsible for the content of the clinical education syllabus related to this clerkship, 
 In addition upon request, UNECOM will provide a clinical clerkship training manual as a reference guide. 
 The above name student is formally enrolled in good standing in the predoctoral program at the University of The New England, College Of Osteopathic Medicine (UNECOM).
 As such she/he is covered, through UNECOM, by a Blanket professional liability insurance policy at $2,000,000/$4,000,000 level.</p>
<p>Student at UNECOM are required to: 
  a) take and pass level 1 COMLEX-USA in order to begin third year clerkship; 
  b) maintain health Insurance coverage while enrolled at the University; 
  c) documents acceptable titers for measles, mumps, rubella and vericella; 
  d) document Hepatitis B series + titer(or Wavier); 
  e) documents annual tuberculosis screening; 
  f) documents current tetanus vaccine (TDAP or TD); 
  g) maintain certification of Basic Life Support And Advanced Cardiac Life Support; 
  h) annually update training regarding HIPPA regulations and OSHA bloodborne pathogens.</p>
<p style="text-align:right; padding-right:10px;margin: 0;">Guy A. DeFeo,</p>
<p style="text-align:right; padding-right:10px;margin: 0;">Associate Dean of Clinical Education</p>
<p style="text-align:right; padding-right:10px;margin: 0;">UNECOM</p>

</div>
</td>
</tr>
</table>
<br><br><br>
<table border="2px">
<tr>
	<td width="400px;" >
		<table border="2px">
			<tr><td width="400px;" ><b>Student:</b> List goals for this clerkship</td></tr>
			<tr><td width="400px;" ><b>1.</b>'.$student_goal_clerkship_1.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>2.</b>'.$student_goal_clerkship_2.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>3.</b>'.$student_goal_clerkship_3.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>Student:</b> Listed Clerkships you will have completed prior to this requested clerkship</td></tr>
			<tr><td width="400px;" ><b>1.</b>'.$student_request_clerkship_1.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>2.</b>'.$student_request_clerkship_2.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>3.</b>'.$student_request_clerkship_3.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>4.</b>'.$student_request_clerkship_4.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>5.</b>'.$student_request_clerkship_5.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>6.</b>'.$student_request_clerkship_6.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>7.</b>'.$student_request_clerkship_7.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>8.</b>'.$student_request_clerkship_8.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>9.</b>'.$student_request_clerkship_9.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>10.</b>'.$student_request_clerkship_10.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>11.</b>'.$student_request_clerkship_11.'&nbsp;</td></tr>
			<tr><td width="400px;" ><b>12.</b>'.$student_request_clerkship_12.'&nbsp;</td></tr>
		</table>
	</td>
	<td width="230px;">For Image:</td>
</tr>

<tr>

<td colspan="2" width="630px;" >
<p>&nbsp;</p>
<p>Submit This Registration A MINIMUM Of 45 Days In Advance Of The Intended Start Of Your Clerkship To :</p>
<p>&nbsp;</p>
<div style="width:630px; float:left; margin-bottom:10px;">

<div style="width:315px; float:left; border-right:2px solid #000; text-align:center;">
<span style="width:315px; float:left; text-align:center;"><b>Judy Beauchemin</b></span><br />
<span style="width:315px; float:left;  text-align:center;"><b>jbeauchemin@une.edu</b></span><br />
<span style="width:315px; float:left;  text-align:center;"><b>Tel: </b>207-602-2674</span><br />
</div>
<div style="width:315px; float:left; text-align:center;">
<span style="width:315px; float:left; text-align:center;"><b>Jacqueline Hawkins</b></span><br />
<span style="width:315px; float:left;text-align:center;"><b>jhawkins1@une.edu</b></span><br />
<span style="width:315px; float:left; text-align:center;"><b>Tel: </b>207-602-2527</span><br />
</div>

</div>
</td>


</tr>

</table>

</div>

</body>

</html>';


// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $htm, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
if(isset($_GET['print']) && $_GET['print']==1) {
	$pdf->IncludeJS("print();");
}
$pdf->Output('/home/nerd/public_html/laravel/pdf/'.$une_info['pdfname'], 'F');

//============================================================+
// END OF FILE
//============================================================+

