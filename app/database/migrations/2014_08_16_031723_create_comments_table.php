<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('record_id');
			$table->integer('eval_id');
			$table->integer('site_id')->index();
			$table->string('service_name');
			$table->date('start_date');
			$table->date('end_date');
			$table->string('strengths')->nullable();
			$table->string('improvements')->nullable();
			$table->string('typical_day')->nullable();
			$table->string('overall')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
