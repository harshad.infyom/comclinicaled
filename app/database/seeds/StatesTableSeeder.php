<?php

use UNE\Models\State;
use UNE\Models\Discipline;

class StatesTableSeeder extends Seeder {

    public function run() {
        DB::table('states')->truncate();
        DB::table('discipline_state')->truncate();

        $state = State::create(["short" => "", "name" => "All"]);
        $d     = [];
        foreach (range(1, 176) as $i) {
            array_push($d, $i);
        }
        $state->disciplines()->sync($d);

        $state = State::create(["short" => "AL", "name" => "Alabama"]);
        $state->disciplines()->sync([1, 2, 3, 5, 50, 40, 20]);

        $state = State::create(["short" => "AK", "name" => "Alaska"]);
        $state->disciplines()->sync([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 30, 103]);

        $state = State::create(["short" => "AZ", "name" => "Arizona"]);
        $state->disciplines()->sync([1, 25]);

        $state = State::create(["short" => "AR", "name" => "Arkansas"]);
        $state = State::create(["short" => "CA", "name" => "California"]);
        $state = State::create(["short" => "CO", "name" => "Colorado"]);
        $state = State::create(["short" => "CT", "name" => "Connecticut"]);
        $state = State::create(["short" => "DE", "name" => "Delaware"]);
        $state = State::create(["short" => "DC", "name" => "District Of Columbia"]);
        $state = State::create(["short" => "FL", "name" => "Florida"]);
        $state = State::create(["short" => "GA", "name" => "Georgia"]);
        $state = State::create(["short" => "HI", "name" => "Hawaii"]);
        $state = State::create(["short" => "ID", "name" => "Idaho"]);
        $state = State::create(["short" => "IL", "name" => "Illinois"]);
        $state = State::create(["short" => "IN", "name" => "Indiana"]);
        $state = State::create(["short" => "IA", "name" => "Iowa"]);
        $state = State::create(["short" => "KS", "name" => "Kansas"]);
        $state = State::create(["short" => "KY", "name" => "Kentucky"]);
        $state = State::create(["short" => "LA", "name" => "Louisiana"]);
        $state = State::create(["short" => "ME", "name" => "Maine"]);
        $state = State::create(["short" => "MD", "name" => "Maryland"]);
        $state = State::create(["short" => "MA", "name" => "Massachusetts"]);
        $state = State::create(["short" => "MI", "name" => "Michigan"]);
        $state = State::create(["short" => "MN", "name" => "Minnesota"]);
        $state = State::create(["short" => "MS", "name" => "Mississippi"]);
        $state = State::create(["short" => "MO", "name" => "Missouri"]);
        $state = State::create(["short" => "MT", "name" => "Montana"]);
        $state = State::create(["short" => "NE", "name" => "Nebraska"]);
        $state = State::create(["short" => "NV", "name" => "Nevada"]);
        $state = State::create(["short" => "NH", "name" => "New Hampshire"]);
        $state = State::create(["short" => "NJ", "name" => "New Jersey"]);
        $state = State::create(["short" => "NM", "name" => "New Mexico"]);
        $state = State::create(["short" => "NY", "name" => "New York"]);
        $state = State::create(["short" => "NC", "name" => "North Carolina"]);
        $state = State::create(["short" => "ND", "name" => "North Dakota"]);
        $state = State::create(["short" => "OH", "name" => "Ohio"]);
        $state = State::create(["short" => "OK", "name" => "Oklahoma"]);
        $state = State::create(["short" => "OR", "name" => "Oregon"]);
        $state = State::create(["short" => "PA", "name" => "Pennsylvania"]);
        $state = State::create(["short" => "RI", "name" => "Rhode Island"]);
        $state = State::create(["short" => "SC", "name" => "South Carolina"]);
        $state = State::create(["short" => "SD", "name" => "South Dakota"]);
        $state = State::create(["short" => "TN", "name" => "Tennessee"]);
        $state = State::create(["short" => "TX", "name" => "Texas"]);
        $state = State::create(["short" => "UT", "name" => "Utah"]);
        $state = State::create(["short" => "VT", "name" => "Vermont"]);
        $state = State::create(["short" => "VA", "name" => "Virginia"]);
        $state = State::create(["short" => "WA", "name" => "Washington"]);
        $state = State::create(["short" => "WV", "name" => "West Virginia"]);
        $state = State::create(["short" => "WI", "name" => "Wisconsin"]);
        $state = State::create(["short" => "WY", "name" => "Wyoming"]);
    }

} 