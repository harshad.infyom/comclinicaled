<?php

// Composer: "fzaninotto/faker": "v1.3.0"

use Cartalyst\Sentry\Facades\Laravel\Sentry;

class UserTableSeeder extends Seeder {

    public function run() {
        // Create the group
        try {
            $admin = Sentry::findGroupByName('Administrator');
        } catch (\Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            $admin = Sentry::createGroup(array(
                                             'name'        => 'Administrator',
                                             'permissions' => array(
                                                 'admin' => 1,
                                                 'users' => 1,
                                             ),
                                         ));
        }

        try {
            $member_group = Sentry::findGroupByName('Member');
        } catch (\Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            $member_group = Sentry::createGroup(array(
                                             'name'        => 'Member',
                                             'permissions' => array(
                                                 'users' => 1
                                             ),
                                         ));
        }




        $user = Sentry::createUser([
                                       'email'      => 'sshekarsiri@gmail.com',
                                       'password'   => '1234',
                                       'activated'  => true,
                                       'first_name' => 'Shekar',
                                       'last_name'  => 'Siri',
                                       'prn'        => '132155321'
                                   ]);


        $user->addGroup($admin);

    }

}