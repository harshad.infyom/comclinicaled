<?php
namespace UNEFront;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class UNEFrontServiceProvider extends ServiceProvider {

    public function boot() {
        View::addLocation(__DIR__ . '/views');
        include __DIR__ . '/filters.php';
        include __DIR__ . '/routes.php';

    }


    public function register() {
        // TODO: Implement register() method.
    }

} 