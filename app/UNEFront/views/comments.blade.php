@extends('layouts.master')


@section('content')
<style type="text/css">
    .scrollToTop{
        z-index: 100;
        /*width:100px; */
        /*height:50px;*/
        background-color: #339933;
        color: #FFF;
        padding:10px; 
        text-align:center; 
        /*background: whiteSmoke;*/
        /*font-weight: bold;*/
        text-decoration: none;
        position:fixed;
        bottom: 81px;
        right:17px;
        font-size: 20px;
        padding: 4px 8px!important;
        /*display: block;*/
        margin: 0;
        display:none;
        /*background: url('/img/arrow_up.png') no-repeat 0px 20px;*/
    }
    .scrollToTop:hover{
        text-decoration:none;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){
    
    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
        if ($(this).scrollTop() > 200) {
            //alert('ok');
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
    
    //Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });
    
});
</script>
<a href="#" class="scrollToTop"><i class="glyphicon-circle_arrow_top"></i></a>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="glyphicon-circle_question_mark"></i>
                    Rotational Site Review
                </h3>

            </div>

            <div class="col-sm-12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="glyphicon-hospital_h"></i>
                            {{ $review->name }}
                        </h3>
                        <div class="pull-right">
                            <a href="{{ URL::previous() }}" class="btn btn-default">Back to Results</a>
                            {{ link_to_route('searchreviews', 'Search Again', null, ['class' => 'btn btn-default', 'style' => 'margin-right:20px;']) }}
                        </div>

                    </div>
                    <div class="box-content nopadding">
                        <ul class="timeline">

                            @if($review->comments->count())
                            @foreach($review->comments as $comment)
                            <li>
                                <div class="timeline-content">
                                    <div class="left">
                                        <div class="icon green">
                                            <i class="fa fa-comment"></i>
                                        </div>
                                        <div class="date">{{ date('M d Y', strtotime($comment->start_date)) }}</div>
                                    </div>
                                    <div class="activity">
                                        <div class="user"><h4>{{ $comment->service_name }}</h4></div>
                                        <p>
                                            <h4>Strengths:</h4>

                                            <p>{{ $comment->strengths }}</p>

                                            <h4>Improvements:</h4>

                                            <p>{{ $comment->improvements }}</p>
                                            <h4>Typical Day:</h4>

                                            <p>{{ $comment->typical_day }}</p>
                                            
                                            <h4>Overall:</h4>

                                            <p>{{ $comment->overall }}</p>
                                        </p>
                                    </div>
                                </div>
                                <div class="line"></div>
                            </li>
                            @endforeach
                            @else

                            <li>
                                <div class="alert alert-danger" style="margin-right: 30px;">
                                    Sorry there is currently no comments for this location.
                                </div>
                            </li>
                            @endif

                        </ul>
                    </div>
                </div>

                {{ link_to_route('searchreviews', 'Search Again', null, ['class' => 'pull-right btn btn-default', 'style' => 'margin-right:20px; margin-top:20px;']) }}
            </div>
        </div>
    </div>
</div>

@stop