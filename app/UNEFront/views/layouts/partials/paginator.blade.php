<?php
    $presenter = new UNE\Services\Presenters\UNEPaginatorPresenter( $paginator );
?>

<?php if ($paginator->getLastPage() > 1): ?>
    <ul class="pagination pagination-custom">
        <?php echo $presenter->render(); ?>
    </ul>
<?php endif; ?>