<div id="navigation">
    <div class="container-fluid">


        <ul class='main-nav'>
            <li>
                {{ link_to_route('home', 'Dashboard') }}
            </li>
            <li>
                {{ link_to_route('searchreviews', "Search Rotation Reviews") }}
            </li>
            <li>
                {{ link_to_route('une', "Clerkship Registration") }}
            </li>
            <li>
                {{ link_to_route('favorites', 'View Your Favorites') }}
            </li>
            <!-- <li>
                <a href="#">
                    <span>Return to BlackBoard&reg;</span>
                </a>
            </li> -->
            <li>
                {{-- link_to_route('profile', 'Edit Profile') --}}
            </li>
            <li>
                {{ link_to_route('logout', 'Sign Out') }}
            </li>

            @if($currentUser->hasAccess('admin'))
                <li>
                    {{ link_to_route('adminHome', 'Admin') }}
                </li>

                <li>
                                    {{ link_to_route('admin.states.index', 'States & Disciplines') }}
                                </li>
            @endif


        </ul>
        <div class="user">
            <ul class="icon-nav">

                <div class="dropdown">
                    <a href="#" class='dropdown-toggle' data-toggle="dropdown"></a>
                    <ul class="dropdown-menu pull-right">

                    </ul>
                </div>
        </div>
    </div>
</div>
<div class="container-fluid nav-hidden" id="content">
    <div id="left">

        <div class="subnav subnav-hidden">
        </div>
    </div>
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1><img src="{{ asset('img/une.png') }}" alt="" class='retina-ready'></h1>
                </div>
                <div class="pull-right">
                    <h2>Welcome {{ $currentUser->full_name() }}</h2>
                </div>
            </div>
        </div>