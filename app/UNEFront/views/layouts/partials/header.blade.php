<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <!-- Apple devices fullscreen -->
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent"/>

    <title>University of New England</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="{{ asset('css/plugins/jquery-ui/smoothness/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/plugins/jquery-ui/smoothness/jquery.ui.theme.css') }}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- Color CSS -->
    <link rel="stylesheet" href="{{ asset('css/themes.css') }}">


    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Nice Scroll -->
    <script src="{{ asset('js/plugins/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <!-- imagesLoaded -->
    <script src="{{ asset('js/plugins/imagesLoaded/jquery.imagesloaded.min.js') }}"></script>
    <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery.ui.core.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-ui/jquery.ui.widget.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-ui/jquery.ui.mouse.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-ui/jquery.ui.resizable.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-ui/jquery.ui.sortable.min.js') }}"></script>
    <!-- slimScroll -->
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Bootbox -->
    <script src="{{ asset('js/plugins/bootbox/jquery.bootbox.js') }}"></script>
    <!-- Bootbox -->
    <script src="{{ asset('js/plugins/form/jquery.form.min.js') }}"></script>
    <!-- Form -->
    <script src="{{ asset('js/plugins/form/jquery.form.min.js') }}"></script>
    <!-- Wizard -->
    <script src="{{ asset('js/plugins/wizard/jquery.form.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/mockjax/jquery.mockjax.js') }}"></script>
    <!-- Validation -->
    <script src="{{ asset('js/plugins/validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/validation/additional-methods.min.js') }}"></script>

    <!-- Theme framework -->
    <script src="{{ asset('js/eakroko.min.js') }}"></script>
    <!-- Theme scripts -->
    <script src="{{ asset('js/application.min.js') }}"></script>
    <!-- Just for demonstration -->
    <script src="{{ asset('js/demonstration.min.js') }}"></script>
    <script src="{{ asset('js/angular.min.js') }}"></script>

    <!--[if lte IE 9]>
    <script src="{{ asset('js/plugins/placeholder/jquery.placeholder.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('input, textarea').placeholder();
        });
    </script>
    <![endif]-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="http://www.une.edu/sites/all/themes/une_theme/favicon.ico"/>
    <!-- Apple devices Homescreen icon -->
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png"/>

</head>
<body>