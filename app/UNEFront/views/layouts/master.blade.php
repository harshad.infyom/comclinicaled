@include('layouts.partials.header')

<!-- Navigation -->
@include('layouts.partials.nav')
<!-- Navigation END -->


<!-- CONTENT -->
@include('flash::message')
@yield('content')
<!-- CONTENT END -->

<script>$('#flash-overlay-modal').modal()</script>
@yield('script')
@include('layouts.partials.footer')