@extends('layouts.default')

@section('content')
<body class='login'>
<div class="wrapper">
    <h1><img src="img/une.png" alt="" class='retina-ready'></h1>

    <div class="login-body">
        <h2>SIGN IN</h2>


        {{ Form::open(['url' => 'login', 'class' => 'form-validate', 'id' => 'test']) }}
        <!--            <form action="index.html" method='get' class='form-validate' id="test"> -->
        <div class="form-group">
            <div class="prn controls">
                <input type="text" name='prn' placeholder="PRN Number" class='form-control'
                       data-rule-required="true">
            </div>
        </div>
<!--        <div class="form-group">-->
<!--            <div class="email controls">-->
<!--                <input type="text" name='email' placeholder="Email address" class='form-control'-->
<!--                       data-rule-required="true" data-rule-email="true">-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="form-group">-->
<!--            <div class="pw controls">-->
<!--                <input type="password" name="password" placeholder="Password" class='form-control'-->
<!--                       data-rule-required="true">-->
<!--            </div>-->
<!--        </div>-->
        <div class="submit">


            <input type="submit" value="Sign me in" class='btn btn-primary'>
			<br>
			<br>
			<br>
			<br>
        </div>
        </form>

    </div>
</div>
</body>
@stop