@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="glyphicon-circle_question_mark"></i>
                    What Would You Like To Do?
                </h3>
            </div>
            <div class="box-content">
                <ul class="tiles">
                    <li class="blue long">
                        <a href="searchreviews"><span><i class="glyphicon-search"></i></span><span
                                class='name'>Search Rotation Reviews</span></a>
                    </li>
                    <li class="blue long">
                        <a href="favorites"><span><i class="glyphicon-heart_empty"></i></span><span
                                class='name'>View Favorites</span></a>
                    </li>
                    <!-- <li class="blue long">
                        <a href="#"><span><i class="glyphicon-unshare"></i></span><span
                                class='name'>Return to BlackBoard&reg;</span></a>
                    </li> -->
                    {{-- <li class="blue long">
                        <a href="profile"><span><i class="glyphicon-user"></i></span><span
                                class='name'>Edit Profile</span></a>
                    </li> --}}
                    <li class="blue long">
                        <a href="une"><span class='name'>Submit  New Clerkship Request and Approval  Form</span></a>
                    </li>
                    <li class="blue long">
                        <a href="login"><span><i class="glyphicon-keys"></i></span><span class='name'>Logout</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@stop