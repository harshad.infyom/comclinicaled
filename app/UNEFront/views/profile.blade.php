@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">

        @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            <ul>
                <li>{{ $error }}</li>
            </ul>
            @endforeach
        </div>
        @endif


        <div class="col-sm-12">
            <div class="box box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="fa fa-user"></i>
                        Edit Profile
                    </h3>
                </div>
                <div class="box-content nopadding">
<!--                 <form action="#" method="POST" class='form-horizontal form-bordered'>-->
                     {{ Form::open(['route' => 'profile_update', 'class' => 'form-horizontal form-bordered']) }}

                        <div class="form-group">
                            <label for="first_name" class="control-label col-sm-2">First Name</label>

                            <div class="col-sm-10">
                                {{ Form::text('first_name', $currentUser->first_name, ['class' => 'form-control']) }}
<!--                                <input type="text" name="first_name" id="first_name" placeholder="John"-->
<!--                                       class="input-xlarge">-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="control-label col-sm-2">Last Name</label>

                            <div class="col-sm-10">
                                {{ Form::text('last_name', $currentUser->last_name, ['class' => 'form-control']) }}
<!--                                <input type="text" name="last_name" id="last_name" placeholder="Doe"-->
<!--                                       class="input-xlarge">-->
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="textfield" class="control-label col-sm-2">Email</label>

                            <div class="col-sm-10">
                                {{ Form::text('email', $currentUser->email, ['class' => 'form-control', 'disabled']) }}
<!--                                <input type="text" name="textfield" id="textfield" placeholder="Email"-->
<!--                                       class="input-xlarge">-->
                            </div>
                        </div>
                                              <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn">Cancel</button>
                            {{-- link_to_route('home', 'Cancel', ['class' => 'btn']) --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop