@extends('layouts.master')

@section('content')
<style type="text/css">
    .btn-favorite {
        font-size: 12px !important;
        text-decoration: none !important;
        margin-left: 10px;
    }
</style>

{{ Form::open(['route' => 'results', 'method' => 'get', 'class' => 'sortForm']) }}
<div class="row">
    <div class="col-sm-12">
        <div class="box">


            <div class="col-sm-12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3><i class="fa fa-search"></i>Search Site Reviews</h3>
                        {{ link_to_route('searchreviews', 'Search Again', null, ['class' => 'pull-right btn btn-default', 'style' => 'margin-right:20px;']) }}
                    </div>
                    <div class="box-content nopadding">
                        <div class="highlight-toolbar">
                            <div class="pull-left">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        {{ $reviews->appends($data)->links() }}
                                    </div>
                                    <div class="btn-group">
                                        <div class="dropdown">

                                            {{ Form::hidden('state', $data['state']) }}
                                            {{ Form::hidden('sort', $data['sort'], ['class' => 'sortField']) }}
                                            {{ Form::hidden('service', $data['service']) }}

                                            <a href="#" class="btn" data-toggle="dropdown" rel="tooltip"
                                               title="Sort">Sort by <span class="caret"></span></a>
                                            <ul class="dropdown-menu sortlinks">
                                                <li><a href="#" data-sort="name">Name</a></li>
                                                <li><a href="#" data-sort="created_at">Date</a></li>
                                                <li><a href="#" data-sort="service">Relevance</a></li>
                                            </ul>

                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="btn-group">
                                    <span>Showing results <strong>{{ $reviews->getFrom() }} - {{ $reviews->getTo() }}</strong> of <strong>{{ $reviews->getTotal() }} </strong></span>
                                </div>
                            </div>
                        </div>
                        <div class="search-results">
                            @if($reviews->count())
                            <ul>
                                @foreach($reviews as $review)
                                <li>

                                    <div class="search-info">

                                        {{ link_to_route('showReview', $review->name, $review->id ) }} {{ link_to_route('favorite', ' Save To Favorites', '', ['class' => 'btn-favorite fa fa-heart favorite', 'data-id' => $review->id ]) }}
                                        <p>{{ $review->street }} {{ $review->street2 }}<br>{{ $review->city }}, {{ $review->state }} {{ $review->zipcode }}<br>
<!--                                            <a href="https://www.google.com/maps/place/Eastern+Maine+Medical+Center/@44.809711,-68.749366,17z/data=!3m1!4b1!4m2!3m1!1s0x4cae4b555f60555b:0x41905b9497ee7085"-->
<!--                                               target="_blank">See Map</a>-->
<!--                                            <a href="https://www.google.com/maps/place/@" . 44809711 . "," . "-68.749366,17z/data=!3m1!4b1!4m2!3m1!1s0x4cae4b555f60555b:0x41905b9497ee7085"-->
<!--                                               target="_blank">See Map</a>-->
                                        </p>

                                    </div>

                                </li>
                                @endforeach
                            </ul>
                            @else
                                <li>
                                    <div class="alert alert-danger" style="margin-right: 30px;">
                                        Sorry there is currently no Reviews.
                                    </div>
                                </li>
                            @endif
                        </div>

                        <div class="highlight-toolbar bottom">
                            <div class="pull-left">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        {{ $reviews->links() }}
                                    </div>
                                    <div class="btn-group">
                                        <div class="dropdown">
                                            <a href="#" class="btn" data-toggle="dropdown" rel="tooltip"
                                               title="Sort">Sort by <span class="caret"></span></a>
                                            <ul class="dropdown-menu sortlinksBottom">
                                                <li><a href="#" data-sort="name">Name</a></li>
                                                <li><a href="#" data-sort="created_at">Date</a></li>
                                                <li><a href="#" data-sort="service">Relevance</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="btn-group">
                                    <span>Showing results <strong>{{ $reviews->getFrom() }} - {{ $reviews->getTo() }}</strong> of <strong>{{ $reviews->getTotal() }} </strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{ link_to_route('searchreviews', 'Search Again', null, ['class' => 'pull-right btn btn-default', 'style' => 'margin-right:20px; margin-top:20px;']) }}
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

<!-- SORT -->
<script>
    $('.sortlinks li a').click(function (e) {
        var val = $(e.target).data('sort');
        $('.sortField').val(val);

        $('.sortForm').submit();

    });

    $('.sortlinksBottom li a').click(function (e) {
        var val = $(e.target).data('sort');
        $('.sortField').val(val);

        $('.sortForm').submit();

    });
</script>


<!-- Favourite -->
<script>
    $('.favorite').click(function (e) {
        e.preventDefault();
        var site_id = $(e.target).data('id');
        var request = $.ajax({
            url: '/favorite',
            data: {site_id: site_id}
        });

        request.success(function (data) {
            alert('Added to favourites!');
        });

        request.error(function (data) {
            alert('Err');
        });
    });
</script>

@stop