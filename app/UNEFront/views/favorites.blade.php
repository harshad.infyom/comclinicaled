@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="glyphicon-heart"></i>
                    Your Favorites
                </h3>
            </div>

            <div class="col-sm-12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-heart"></i>
                            Favorites
                        </h3>
                    </div>
                    <div class="box-content nopadding">
                        <div class="highlight-toolbar">
                            <div class="pull-left">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        {{ $favourites->links() }}
                                    </div>
                                    <div class="btn-group">
                                        <div class="dropdown">
                                            <a href="#" class="btn" data-toggle="dropdown" rel="tooltip" title="Sort">Sort by <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Name</a></li>
                                                <li><a href="#">Date</a></li>
                                                <li><a href="#">Relevance</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="btn-group">
                                    <span>Showing results <strong>{{ $favourites->getFrom() }} - {{ $favourites->getTo() }}</strong> of <strong>{{ $favourites->getTotal() }}</strong></span>
                                </div>
                            </div>
                        </div>
                        <div class="search-results">
                            <ul>
                                @foreach($favourites as $review)
                                <li>
                                    <div class="search-info">
                                        {{ link_to_route('showReview', $review->name, $review->id ) }}
                                        <p>{{ $review->street }} {{ $review->street2 }}<br>{{ $review->city }}, {{ $review->state }} {{ $review->zipcode }}<br><a href="https://www.google.com/maps/place/Eastern+Maine+Medical+Center/@44.809711,-68.749366,17z/data=!3m1!4b1!4m2!3m1!1s0x4cae4b555f60555b:0x41905b9497ee7085" target="_blank">See Map</a></p>

                                    </div>

                                </li>
                                @endforeach

                            </ul>
                        </div>
                        <div class="highlight-toolbar bottom">
                            <div class="pull-left">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        {{ $favourites->links() }}
                                    </div>
                                    <div class="btn-group">
                                        <div class="dropdown">
                                            <a href="#" class="btn" data-toggle="dropdown" rel="tooltip" title="Sort">Sort by <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Name</a></li>
                                                <li><a href="#">Date</a></li>
                                                <li><a href="#">Relevance</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="btn-group">
                                    <span>Showing results <strong>{{ $favourites->getFrom() }} - {{ $favourites->getTo() }}</strong> of <strong>{{ $favourites->getTotal() }}</strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop