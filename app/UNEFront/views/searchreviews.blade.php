@extends('layouts.master')


@section('content')
<script type="text/javascript">
    $(document).ready(function(){
        $('#state').change(function(e){
            var selected = $(this).find('option:selected');
            console.log(selected.val());
            console.log($(this));
        });


        var UNE = angular.module('UNE', []);


        UNE.controller('StateController', ['$scope', function($scope){
            $scope.states = {{ json_encode($states->toArray()) }}
            $scope.disciplines = {{ json_encode($disciplines->toArray()) }}

            $scope.search_type = false;
            $scope.selectedState = $scope.states[0];
            $scope.selectedDiscipline = $scope.selectedState.disciplines[0];

            $scope.stateChanged = function(){
                //console.log($scope.search_type);
                $scope.selectedDiscipline = $scope.selectedState.disciplines[0];
            }

            $scope.disciplineChanged = function(){
                $scope.selectedState = $scope.selectedDiscipline.states[0];
            }

            $scope.searchTypeChanged = function() {
                if($scope.search_type) {
                    $scope.selectedDiscipline = $scope.disciplines[0];
                    $scope.selectedState = $scope.selectedDiscipline.states[0];
                } else {
                    $scope.selectedState = $scope.states[0];
                    $scope.selectedDiscipline = $scope.selectedState.disciplines[0];
                }
                //console.log($scope.search_type);
            }
        }]);

        angular.bootstrap($('.ang-app'), ['UNE']);
    })
</script>

<div class="row ang-app" ng-controller="StateController">
    <div class="col-sm-12">

        @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            <ul>
                <li>{{ $error }}</li>
            </ul>
            @endforeach
        </div>
        @endif


        <div class="col-sm-12">
            <div class="box box-bordered">
                <div class="box-title">
                    <h3><i class="fa fa-search"></i>Search Site Reviews</h3>
                </div>
                <div class="box-content nopadding">
                    <!--                 <form action="#" method="POST" class='form-horizontal form-bordered'>-->
                    {{ Form::open(['route' => 'results', 'method' => 'get', 'class' => 'form-horizontal form-bordered']) }}

                    <div class="form-group">
                        <label for="state" class="control-label col-sm-2">How would you like to search?</label>

                        <div class="col-sm-10">
                            <input type="radio" ng-model="search_type" ng-change="searchTypeChanged()" ng-value="false" name="search_type" id="search_type_yes">
                            <label for="search_type_yes">Search by State then Discipline</label>

                            <input type="radio" ng-model="search_type" ng-change="searchTypeChanged()" name="search_type" ng-value="true" id="search_type_no">
                            <label for="search_type_no">Search by Discipline then State</label>
                        </div>
                    </div>

                    <div class="" ng-hide="search_type">
                        <div class="form-group">
                            <label for="state" class="control-label col-sm-2">State Search</label>

                            <div class="col-sm-10">
                                <input type="hidden" name="state" value="@{{ selectedState.name}}">
                                <input type="hidden" name="service" value="@{{ selectedDiscipline.name }}">

                                <select ng-options="state as state.name for state in states" ng-model="selectedState" ng-change="stateChanged()"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="control-label col-sm-2">Discipline Search</label>

                            <div class="col-sm-10">
                                <select ng-options="discipline as discipline.name for discipline in selectedState.disciplines" ng-model="selectedDiscipline"></select>
                            </div>
                        </div>
                    </div>

                    <div class="" ng-show="search_type">
                        <!-- TWO -->
                        <div class="form-group">
                            <label for="last_name" class="control-label col-sm-2">Discipline Search</label>

                            <div class="col-sm-10">
                                <select ng-options="discipline as discipline.name for discipline in disciplines" ng-model="selectedDiscipline" ng-change="disciplineChanged()"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="state" class="control-label col-sm-2">State Search</label>

                            <div class="col-sm-10">
                                <input type="hidden" name="state" value="@{{ selectedState.name}}">
                                <input type="hidden" name="service" value="@{{ selectedDiscipline.name }}">

                                <select ng-options="state as state.name for state in selectedDiscipline.states" ng-model="selectedState"></select>
                            </div>
                        </div>
                    </div>



                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                    {{ Form::hidden('sort', 'name') }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


@stop