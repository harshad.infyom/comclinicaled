@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="glyphicon-circle_question_mark"></i>
                    Rotational Site Review
                </h3>
            </div>

            <div class="col-sm-12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="glyphicon-hospital_h"></i>
                            Eastern Maine Medical Center
                        </h3>
                    </div>
                    <div class="box-content nopadding">
                        <ul class="timeline">
                            <li>
                                <div class="timeline-content">
                                    <div class="left">
                                        <div class="icon green">
                                            <i class="fa fa-comment"></i>
                                        </div>
                                        <div class="date">11/26/12</div>
                                    </div>
                                    <div class="activity">
                                        <div class="user"><h4>Emergency Medicine</h4></div>
                                        <p>
                                        <h4>Strengths:</h4>

                                        <p>Gives students the opportunity to see trauma and follow patients to the operating room.</p>
                                        <h4>Improvements:</h4>

                                        <p>Allow 4th year students to have more responsibilities.</p>
                                        <h4>Comments:</h4>

                                        <p></p>
                                        <h4>Typical Day:</h4>

                                        <p>12 hour shifts. 7p-7a</p>

                                        </p>
                                    </div>
                                </div>
                                <div class="line"></div>
                            </li>

                            <li>
                                <div class="timeline-content">
                                    <div class="left">
                                        <div class="icon green">
                                            <i class="fa fa-comment"></i>
                                        </div>
                                        <div class="date">8/6/12</div>
                                    </div>
                                    <div class="activity">
                                        <div class="user"><h4>Core Family Medicine</h4></div>
                                        <p>
                                        <h4>Strengths:</h4>

                                        <p>Lots of patient contact. Supportive environment. Initiative appreciated. Friendly, casual staff relationships. First name basis. Patient with student mistakes and willing to correct constructively. No high pressure demands, low stress. Freedom to tailor the learning experience to my own needs. The nursing staff is WONDERFUL -- be sure to tell them how much you appreciate them.</p>
                                        <h4>Improvements:</h4>

                                        <p>Encourage faculty and/or residents to ask the student more questions, especially pertaining to the assessment and plan of the patient- it would really help to apply our medical knowledge when it comes to forming differential diagnosis and treatment plan.</p>
                                        <h4>Comments:</h4>

                                        <p>the Boone House is the place to live -- clean, modern, comfortable. EMMC is a great place to rotate and I loved every minute there.</p>
                                        <h4>Typical Day:</h4>

                                        <p>I was scheduled to meet with a number of providers over 6 weeks, so usually I'm come in around 7.30-45 and look up my provider on the computer and get a print out of the day's patients. If I had time, I'd look over each chart to get a basic idea of what their history was like, what their current meds were, and what kinds of complaints they usually had. Then I'd locate my resident and report in. Most days the resident would have me go in and see the patient, get a current history, and then either report back to them or they would join us shortly. We'd examine the patient and then I'd go on to the next one, etc.</p>

                                        </p>
                                    </div>
                                </div>
                                <div class="line"></div>
                            </li>
                            <li>
                                <div class="timeline-content">
                                    <div class="left">
                                        <div class="icon green">
                                            <i class="fa fa-comment"></i>
                                        </div>
                                        <div class="date">8/1/12</div>
                                    </div>
                                    <div class="activity">
                                        <div class="user"><h4>Core Internal Medicine</h4></div>
                                        <p>
                                        <h4>Strengths:</h4>

                                        <p>This clinical experience provided a great opportunity for medical students to become familiar with all aspects of adult inpatient medical care. This rotation also gave medical students adequate time to work with attendings and residents. I particularly enjoyed all of the lectures that were given throughout this rotation by different physicians. I also enjoyed the opportunity to rotate through different services while on Internal Medicine, including palliative care, rehabilitation medicine, and EMIC.</p>
                                        <h4>Improvements:</h4>

                                        <p>It would be nice to have more of a discount at the cafeteria or to have meals included, as is the case at Berkshires Medical Center. A little more involvement by the osteopathic fellows on a daily basis would be nice. I rarely did OMT until the last couple of weeks because the felllows, and therefore, supervisors, weren't a presence, even though they said they would be. Probably, inpatient OMT is not their primary responsibility, but it's still something of a minor point to address.</p>
                                        <h4>Comments:</h4>

                                        <p>EMMC is a great hospital for rotations and I feel I learned a great deal that I'll use every day for the rest of my life -- I'm serious about that. This program exceeded my expectations far and above anything I could have imagined and I just wish they had a residency program in my chosen area; I'd apply today if I could.</p>
                                        <h4>Typical Day:</h4>

                                        <p>"On the resident teaching service, you start at 7:00 AM with rounds in B-5, the resident's lounge (it's also your lounge as a student, so use it as a place to study, have lunch, etc. but clean up after yourself and be considerate); afterwards, you either go forth with your resident or you go forth alone, to see patients, knowing you'll follow-up with your resident afterward. Each patient requires a clinical note. The first 6 weeks are primarily about learning how to write a good one. Your resident will help you; look at her/his notes in the chart and then write a note and ask them for comments. Work hard on developing your own assessments as well as plan for the patient each day but also compare yours with the resident's and make sure your plan does not deviate too radically from theirs. If it does, you've ""misdiagnosed"" the patient. You'll have time to study most days or to go with your patients to procedures or to work on new admissions. Don't hesitate to ask your intern/resident if there is anything you can do to help them. Study your patients' charts in the electronic medical record. Know them inside and out, look up their pathology in Up to Date, be ready to offer comments during evening rounds at 6:00 PM.

                                            With the hospitalist service, you spend an average of 8 days (including one weekend) following one of the MD hospitalists. I loved this part of the rotation and found Dr. Blejeru to be an outstanding instructor, a good deal of fun, and very fair. Overall it was a tremendous confidence building experience.

                                            Rehab is very interesting, fun, and a break from the other services because you spend less than 8 hours, most days, either in the outpatient clinic or inpatient floors, and frankly, you can use the break. The rest of the services work you very hard but of course, that's why we're here, too. :-)"</p>

                                        </p>
                                    </div>
                                </div>
                                <div class="line"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop