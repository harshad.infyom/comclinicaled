<?php

Route::group(['before' => 'Sentry'], function () {
    Route::get('/', [
        'as'   => 'home',
        'uses' => 'UNEFront\Controllers\PagesController@index'
    ]);

    Route::get('une', array('as' => 'une','uses' => 'UNEFront\Controllers\UneController@getRegForm'));

    Route::post('une', 'UNEFront\Controllers\UneController@storeUne');

    // Route::get('profile', ['as' => 'profile', 'uses' => 'UNEFront\Controllers\UserController@index']);
    // Route::post('profile', ['as' => 'profile_update', 'uses' => 'UNEFront\Controllers\UserController@update']);



    Route::get('results/{id}', ['as' => 'showReview', 'uses' => 'UNEFront\Controllers\ReviewsController@show']);
    Route::get('results', ['as' => 'results', 'uses' => 'UNEFront\Controllers\ReviewsController@search']);

    Route::get('searchreviews', ['as' => 'searchreviews', 'uses' => 'UNEFront\Controllers\ReviewsController@index']);
    Route::post('searchreviews', ['as' => 'searchreviewsPost', 'uses' => 'UNEFront\Controllers\ReviewsController@search']);

    Route::get('favorite', ['as' => 'favorite', 'uses' => 'UNEFront\Controllers\FavoriteController@create']);
    Route::get('favorites', ['as' => 'favorites', 'uses' => 'UNEFront\Controllers\FavoriteController@index']);

    Route::get('sitereview', function () {
        return View::make('site_review');
    });
});


Route::get('login', ['as' => 'login', 'uses' => 'UNEFront\Controllers\SessionController@create']);
Route::get('logout', ['as' => 'logout', 'uses' => 'UNEFront\Controllers\SessionController@destroy']);
Route::post('login', 'UNEFront\Controllers\SessionController@store');


