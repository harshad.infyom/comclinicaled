<?php


namespace UNEFront\Controllers;


use Input;
use View;
use UserService;

class FavoriteController extends \BaseController {

    public function index() {
        $favourites = UserService::getFavourites();

        //dd($favourites->toArray());
        return View::make('favorites', compact('favourites'));
    }

    public function create() {
        $site_id = Input::get('site_id');

        $fav = UserService::addFavourite($site_id);

        return $fav;
    }
} 