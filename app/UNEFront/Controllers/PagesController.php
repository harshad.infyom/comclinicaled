<?php


namespace UNEFront\Controllers;


use View;

class PagesController extends \BaseController {

    public function index() {
        return View::make("index");
    }
}