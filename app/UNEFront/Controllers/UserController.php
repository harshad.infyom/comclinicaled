<?php


namespace UNEFront\Controllers;


use Illuminate\Support\Facades\Redirect;
use Input;
use Laracasts\Flash\Flash;
use UserService;
use View;

class UserController extends \BaseController {
    public function index(){
        return View::make('profile');
    }



    public function update(){
        $data = Input::only('first_name', 'last_name');

        $user = UserService::update($data);

        Flash::overlay('Profile updated successfully!');
        return Redirect::route('profile');
    }
} 