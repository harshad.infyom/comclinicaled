<?php


namespace UNEFront\Controllers;


use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Cartalyst\Sentry\Throttling\UserBannedException;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\UserNotActivatedException;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Illuminate\Support\Facades\Redirect;
use Input;
use View;

class SessionController extends \BaseController {

    public function store() {
        //$credentials = Input::only('email', 'password');
        $credentials               = Input::only('prn');
        $credentials[ 'password' ] = '1234';

        try {
            $user = Sentry::authenticate($credentials);
            // Find the user using the user id
            //$user = Sentry::findUserById(1);
            // Log the user in
            //Sentry::login($user, false);
        } catch (LoginRequiredException $e) {
            return Redirect::back()->withErrors('Login field is required')->withInput();
        } catch (UserNotFoundException $e) {
            return Redirect::back()->withErrors('User not found.')->withInput();
        } catch (UserNotActivatedException $e) {
            return Redirect::back()->withErrors('User not activated.')->withInput();
        } // Following is only needed if throttle is enabled
        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            $time = $throttle->getSuspensionTime();

            return Redirect::back()->withErrors("User is suspended for [$time] minutes.")->withInput();
        } catch (UserBannedException $e) {
            return Redirect::back()->withErrors('User is banned.')->withInput();
        }

        return Redirect::route('home');
    }

    public function create() {
        return View::make('login');
    }

    public function destroy() {
        Sentry::logout();

        return Redirect::route('login');
    }
}


