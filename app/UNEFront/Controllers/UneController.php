<?php
namespace UNEFront\Controllers;

use App;
use DOMPDF;
use Exception;
use Input;
use Redirect;
use UNE\Models\Registration;
use Validator;
use View;

class UneController extends \BaseController
{


    /*
    |--------------------------------------------------------------------------
    | Default Une Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |
    |	Route::get('une', 'UneController@showUne');

    */
    public function getRegForm()
    {
        return View::make('une.index', ['une_info' => []]);
    }

    public function storeUne()
    {
        $rules = array(
            'student_name' => 'required',
            'student_email' => 'required',
            'student_curr_address' => 'required',
            'student_city' => 'required',
            'student_state' => 'required',
            'student_zip' => 'required',
            'prn' => 'required',
            'clerkship_name' => 'required',
            'clerkship_date_to' => 'required',
            'clerkship_date_from' => 'required',
            'clerkship_type' => 'required',
            'clerkship_credit_type' => 'required',
            'site_name' => 'required',
            'site_address' => 'required',
            'site_city' => 'required',
            'site_state' => 'required',
            'site_zip' => 'required',
            'site_contact_name' => 'required',
            'site_name_dept' => 'required',
            'unecom_dat_rec' => 'required',
        );


        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('une')->withErrors($validator);
        }
        // insert value in table
        $une_info = array();
        $st_name = '';
        $mil = '';
        $addres = '';
        $une_info['download_msg'] = '';
        $une_info['res'] = '';

        if (Input::get('subform')) {
            $student_phone = Input::get('student_phone1') . Input::get('student_phone2') . Input::get('student_phone3');
            $site_phone = Input::get('site_phone') . Input::get('site_phone1') . Input::get('site_phone2');
            $site_preceptor_conf_phone = Input::get('site_preceptor_conf_phone') . Input::get('site_preceptor_conf_phone1') . Input::get('site_preceptor_conf_phone2');
            $site_fax = Input::get('site_fax') . Input::get('site_fax1') . Input::get('site_fax2');
            $stud_info = array(
                'token' => Input::get('_token'),
                'student_name' => Input::get('student_name'),
                'student_grand_year' => Input::get('student_grand_year'),
                'student_phone' => $student_phone,
                'prn' => Input::get('prn'),
                'student_email' => Input::get('student_email'),
                'student_curr_address' => Input::get('student_curr_address'),
                'student_city' => Input::get('student_city'),
                'student_state' => Input::get('student_state'),
                'student_zip' => Input::get('student_zip'),
                'clerkship_name' => Input::get('clerkship_name'),
                'clerkship_date_to' => Input::get('clerkship_date_to'),
                'clerkship_date_from' => Input::get('clerkship_date_from'),
                'clerkship_type' => Input::get('clerkship_type'),
                'clerkship_credit_type' => Input::get('clerkship_credit_type'),
                'clerkship_degree_trainer' => Input::get('clerkship_degree_trainer'),
                'clerkship_clinical_email' => Input::get('clerkship_clinical_email'),
                'site_name' => Input::get('site_name'),
                'site_address' => Input::get('site_address'),
                'site_city' => Input::get('site_city'),
                'site_state' => Input::get('site_state'),
                'site_zip' => Input::get('site_zip'),
                'site_diff_city' => Input::get('site_diff_city'),
                'site_diff_state' => Input::get('site_diff_state'),
                'site_diff_zip' => Input::get('site_diff_zip'),
                'site_contact_name' => Input::get('site_contact_name'),
                'site_email' => Input::get('site_email'),
                'site_phone' => $site_phone,
                'site_fax' => $site_fax,
                'site_name_dept' => Input::get('site_name_dept'),
                'unecom_clerkship' => Input::get('unecom_clerkship'),
                'unecom_dat_rec' => Input::get('unecom_dat_rec'),
                'unecom_of_week' => Input::get('unecom_of_week'),
                'unecom_by_week' => Input::get('unecom_by_week'),
                'unecom_maild' => Input::get('unecom_maild'),
                'site_preceptor_conf_name' => Input::get('site_preceptor_conf_name'),
                'site_preceptor_conf_certified' => Input::get('site_preceptor_conf_certified'),
                'site_preceptor_conf_internship' => Input::get('site_preceptor_conf_internship'),
                'site_preceptor_conf_from' => Input::get('site_preceptor_conf_from'),
                'site_preceptor_conf_to' => Input::get('site_preceptor_conf_to'),
                'site_preceptor_conf_clship' => Input::get('site_preceptor_conf_clship'),
                'site_preceptor_conf_by' => Input::get('site_preceptor_conf_by'),
                'site_preceptor_conf_email' => Input::get('site_preceptor_conf_email'),
                'site_preceptor_conf_phone' => $site_preceptor_conf_phone,
                'site_preceptor_conf_dt' => Input::get('site_preceptor_conf_dt'),
                'site_preceptor_eval_namdegree' => Input::get('site_preceptor_eval_namdegree'),
                'site_preceptor_eval_email' => Input::get('site_preceptor_eval_email'),
                'site_preceptor_eval_name' => Input::get('site_preceptor_eval_name'),
                'site_preceptor_eval_email_1' => Input::get('site_preceptor_eval_email_1'),
                'student_goal_clerkship_1' => Input::get('student_goal_clerkship_1'),
                'student_goal_clerkship_2' => Input::get('student_goal_clerkship_2'),
                'student_goal_clerkship_3' => Input::get('student_goal_clerkship_3'),
                'student_request_clerkship_1' => Input::get('student_request_clerkship_1'),
                'student_request_clerkship_2' => Input::get('student_request_clerkship_2'),
                'student_request_clerkship_3' => Input::get('student_request_clerkship_3'),
                'student_request_clerkship_4' => Input::get('student_request_clerkship_4'),
                'student_request_clerkship_5' => Input::get('student_request_clerkship_5'),
                'student_request_clerkship_6' => Input::get('student_request_clerkship_6'),
                'student_request_clerkship_7' => Input::get('student_request_clerkship_7'),
                'student_request_clerkship_8' => Input::get('student_request_clerkship_8'),
                'student_request_clerkship_9' => Input::get('student_request_clerkship_9'),
                'student_request_clerkship_10' => Input::get('student_request_clerkship_10'),
                'student_request_clerkship_11' => Input::get('student_request_clerkship_11'),
                'student_request_clerkship_12' => Input::get('student_request_clerkship_12'),
                'reg_date' => date('d/m/yy')
            );

            $une_info['res'] = $record = Registration::create($stud_info);
            $last_id = $record->id;

            $pdfname = 'pdf_' . $last_id . '.pdf';

            $pdfurl = 'http://nerdthemes.co/laravel/pdf/' . $pdfname;
            $file = public_path() . DIRECTORY_SEPARATOR . $pdfname;
            $une_info['download_msg'] = '<a href="' . $pdfurl . '" target="_blank;">Download</a> &nbsp; <a href="http://nerdthemes.co/laravel/pdf/printpdf.php?file=' . $pdfurl . '" target="_blank;">Print</a>';
         $this->pdfUne($pdfname, $record);

            $st_name = Input::get('student_name');
            $mil = Input::get('student_email');
            $addres = Input::get('student_curr_address');
            $from_name = 'Nerd';
            $from_mail = 'jbeauchemin@une.edu';
            $replyto = 'jbeauchemin@une.edu';
            //$mailto = "Jhawkins1@une.edu , JBeauchemine@une.edu,".Input::get('student_email');
            $mailto = "mitch.wakem@gmail.com" . Input::get('student_email');
            $subject = "New submission alerts ";

            $message = "
		<html>
		<head>
		<title>New submission alerts </title>
		</head>
		<body>
		<p>This email is inform to you ." . $st_name . " is register on your server.</p>
		<table>
		<tr>
		<th>Email</th>
		<th>Address</th>
		</tr>
		<tr>
		<td>" . $mil . "</td>
		<td>" . $addres . "</td>
		</tr>
		</table>
		</body>
		</html>
		";

            try {
                $filename = $pdfname;
                $une_info['file_path']=$filename;
                $file_size = filesize($file);
                $handle = fopen($file, "r");
                $content = fread($handle, $file_size);
                fclose($handle);
                $content = chunk_split(base64_encode($content));

                $uid = md5(uniqid(time()));

                $header = "From: " . $from_name . " <" . $from_mail . ">\r\n";
                $header .= "Reply-To: " . $replyto . "\r\n";
                $header .= "MIME-Version: 1.0\r\n";
                $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
                $header .= "This is a multi-part message in MIME format.\r\n";
                $header .= "--" . $uid . "\r\n";
                $header .= "Content-type:text/html; charset=iso-8859-1\r\n";
                $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
                $header .= $message . "\r\n\r\n";
                $header .= "--" . $uid . "\r\n";
                $header .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
                $header .= "Content-Transfer-Encoding: base64\r\n";
                $header .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
                $header .= $content . "\r\n\r\n";
                $header .= "--" . $uid . "--";

                // Messages for testing only, nobody will see them unless this script URL is visited manually
                mail($mailto, $subject, "", $header);

            }catch(Exception $e){
                return View::make('une.index', ['une_info' => $une_info])->withErrors('mail not sent due to '.$e->getMessage());
            }
        }


        return View::make('une.index', ['une_info' => $une_info]);
        // Always set content-type when sending HTML email


    }

    /**
     * @param $txt
     * @param Registration $stud_info
     * @return mixed
     */
    public function pdfUne($txt, $stud_info)
    {
//        $une_info['pdfname'] = $txt;
//        $une_info['stud_info'] = $stud_info;
        $pdf = App::make('dompdf');
        $pdf = $pdf->loadView('une.pdf', ['record' => $stud_info]);
        return $pdf->save($txt);
    }

}
