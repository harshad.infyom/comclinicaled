<?php


namespace UNEFront\Controllers;


use Input;
use Review;
use UNE\Models\Discipline;
use UNE\Models\State;
use View;

class ReviewsController extends \BaseController {

    public function index(){
        $states = State::with('disciplines')->has('disciplines')->get();
        //$states_menu = $states->lists('name', 'short');

        $disciplines = Discipline::with('states')->has('states')->get();


        //return $states_menu;
        //$states_data = State::with('disciplines')->get();

        return View::make('searchreviews', compact('states', 'disciplines'));
    }

    public function show($id){
        $review = Review::find($id);
        $review = Review::find($id, ['comments' => function($q) use($review){
                $q->orderBy('start_date', 'DESC')->where('service_name', $review->service)->get();
            }]);
        return View::make('comments', compact('review'));
    }

    public function search(){
        $data = Input::only('state', 'service', 'sort', 'page');

        $with = array('comments');
        $reviews = Review::getSearchReviews($data, $with);

        return View::make('results', compact('reviews', 'data'));
    }
} 