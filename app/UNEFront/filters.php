<?php
/**
 * Sentry filter
 *
 * Checks if the user is logged in
 */
Route::filter('Sentry', function () {
    if (!Sentry::check()) {
        return Redirect::route('login');
    }
});

/**
 * hasAcces filter (permissions)
 *
 * Check if the user has permission (group/user)
 */
Route::filter('hasAccess', function ($route, $request, $value) {
    try {
        $user = Sentry::getUser();

        if (!$user->hasAccess($value)) {
            return Redirect::route('cms.login')->withErrors(array(Lang::get('user.noaccess')));
        }
    } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
        return Redirect::route('cms.login')->withErrors(array(Lang::get('user.notfound')));
    }

});

/**
 * InGroup filter
 *
 * Check if the user belongs to a group
 */
Route::filter('inGroup', function ($route, $request, $value) {
    try {
        $user = Sentry::getUser();

        $group = Sentry::findGroupByName($value);

        if (!$user->inGroup($group)) {
            return Redirect::route('cms.login')->withErrors(array(Lang::get('user.noaccess')));
        }
    } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
        return Redirect::route('cms.login')->withErrors(array(Lang::get('user.notfound')));
    } catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
        return Redirect::route('cms.login')->withErrors(array(Lang::get('group.notfound')));
    }
});


//Example use

Route::group(array('prefix' => 'cms/product', 'before' => 'Sentry|inGroup:Admins'), function () {
    Route::get('/', array(
        'as'     => 'product.index',
        'before' => 'hasAccess:product.index',
        'uses'   => 'ProductController@index'
    ));
});
