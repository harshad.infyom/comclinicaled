<?php


namespace UNE\Repositories\State;


use UNE\Models\State;
use UNE\Repositories\AbstractRepository;

class StateRepositoryEloquent extends AbstractRepository implements StateRepository {

    protected $model;

    function __construct(State $model) {
        $this->model = $model;
    }
} 