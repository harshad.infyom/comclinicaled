<?php


namespace UNE\Repositories;



use stdClass;

abstract class AbstractRepository {
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @param $model
     */
    function __construct($model) {
        $this->model = $model;
    }


    /**
     * @param array $with
     *
     * @return mixed
     */
    public function make(array $with = array()) {
        return $this->model->with($with);
    }


    /**
     * @param array $with
     * @param array $columns
     *
     * @param null  $sort
     *
     * @return mixed
     */
    public function all(array $with = array(), array $columns = array('*'), $sort = null) {
        return $this->make($with)->select($columns)->get();
    }


    /**
     * @param       $id
     * @param array $with
     * @param array $columns
     *
     * @return mixed
     */
    public function find($id, array $with = array(), array $columns = array('*')) {
        return $this->make($with)->select($columns)->find($id);
    }

    /**
     * @param       $column
     * @param       $value
     * @param array $with
     *
     * @return mixed
     */
    public function getWhere($column, $value, array $with = array()) {
        return $this->make($with)->where($column, '=', $value)->first();
    }

    /**
     * @param       $column
     * @param       $value
     * @param array $with
     *
     * @return mixed
     */
    public function getWhereMany($column, $value, array $with = array()) {
        return $this->make($with)->where($column, '=', $value);
    }

    /**
     * @param       $column
     * @param       $value
     * @param array $with
     *
     * @return mixed
     */
    public function getWhereLike($column, $value, array $with = array()) {
        return $this->make($with)->where($column, 'LIKE', "%" . $value . "%");
    }

    /**
     * @param       $limit
     * @param array $with
     *
     * @return mixed
     */
    public function getRecent($limit, array $with = array()) {
        return $this->model->make($with)->orderBy('id', 'DESC')->take($limit);
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data) {
        $model = $this->model->create($data);

        return $model;
    }


    public function firstOrCreate(array $data){
        $model = $this->model->firstOrCreate($data);

        return $model;
    }


    /**
     * @param       $id
     * @param array $data
     *
     * @return bool|int
     */
    public function update($id, array $data) {
        $user = $this->model->find($id);
        $user->fill($data);
        $user->save();
        return $user;
        //return $this->model->update($data);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete($id) {
        return $this->model->destroy($id);
    }

    /**
     * @param $column
     * @param $value
     */
    public function deleteWhere($column, $value) {
        return $this->model->where($column, $value)->destroy();
    }

    /**
     * @param       $key
     * @param       $value
     * @param array $with
     */
    public function getBy($key, $value, array $with = array()) {
        // TODO: Implement getBy() method.
    }

    /**
     * Get Results by Page
     *
     * @param int   $page
     * @param int   $limit
     * @param array $with
     *
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, array $with = array()) {
        $result             = new StdClass;
        $result->page       = $page;
        $result->limit      = $limit;
        $result->totalItems = 0;
        $result->items      = array();

        $query = $this->make($with);

        $users = $query->skip($limit * ( $page - 1 ))
                       ->take($limit)
                       ->get();

        $result->totalItems = $this->model->count();
        $result->items      = $users->all();

        return $result;
    }
} 