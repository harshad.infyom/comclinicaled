<?php


namespace UNE\Repositories;


interface BaseRepository {
    /**
     * @param array $with
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function all(array $with = array(), array $columns = array('*'), $sort = null);

    /**
     * @param       $id
     * @param array $with
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function find($id, array $with = array(), array $columns = array('*'));

    /**
     * @param       $column
     * @param       $value
     * @param array $with
     *
     * @return mixed
     */
    public function getWhere($column, $value, array $with = array());

    /**
     * @param       $column
     * @param       $value
     * @param array $with
     *
     * @return mixed
     */
    public function getWhereMany($column, $value, array $with = array());

    /**
     * @param       $limit
     * @param array $with
     *
     * @return mixed
     */
    public function getRecent($limit, array $with = array());

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);


    /**
     * @param array $data
     *
     * @return mixed
     */
    public function firstOrCreate(array $data);


    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $column
     * @param $value
     *
     * @return mixed
     */
    public function deleteWhere($column, $value);
} 