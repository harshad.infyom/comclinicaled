<?php


namespace UNE\Repositories;


use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use UNE\Models\Comment;
use UNE\Models\Favorite;
use UNE\Models\Review;
use UNE\Models\State;
use UNE\Models\User;
use UNE\Models\UserProfile;
use UNE\Repositories\Comment\CommentRepositoryEloquent;
use UNE\Repositories\Favorite\FavoriteRepositoryEloquent;
use UNE\Repositories\Review\ReviewRepositoryEloquent;
use UNE\Repositories\State\StateRepositoryEloquent;
use UNE\Repositories\User\ProfileRepositoryEloquent;
use UNE\Repositories\User\UserRepositoryEloquent;

class RepositoryServiceProvider extends ServiceProvider {

    protected $app;

    function __construct($app) {
        $this->app = $app;
    }

    public function boot() {
        $this->registerBindings();
        $this->registerAliases();
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        /**
         * USER
         */
        $this->app->bind('UNE\Repositories\User\UserRepository', function ($app) {
            $user = new UserRepositoryEloquent(new User());

            return $user;
        });

        /**
         * User Profile
         */
        $this->app->bind('UNE\Repositories\User\ProfileRepository', function ($app) {
            $profile = new ProfileRepositoryEloquent(new UserProfile());

            return $profile;
        });

        /**
         * Review
         */
        $this->app->bind('UNE\Repositories\Review\ReviewRepository', function ($app) {
            $review = new ReviewRepositoryEloquent(new Review());

            return $review;
        });

        /**
         * Comment
         */
        $this->app->bind('UNE\Repositories\Comment\CommentRepository', function ($app) {
            $comment = new CommentRepositoryEloquent(new Comment());

            return $comment;
        });

        /**
         * Favorite
         */
        $this->app->bind('UNE\Repositories\Favorite\FavoriteRepository', function ($app) {
            $favorite = new FavoriteRepositoryEloquent(new Favorite());

            return $favorite;
        });

        /**
         * State
         */
        $this->app->bind('UNE\Repositories\State\StateRepository', function ($app) {
            $state = new StateRepositoryEloquent(new State());

            return $state;
        });
    }


    private function registerBindings() {
        $this->app->bind('user', 'UNE\Services\UserService');
        $this->app->bind('review', 'UNE\Services\ReviewService');
        $this->app->bind('comment', 'UNE\Services\CommentService');
        $this->app->bind('state', 'UNE\Services\StateService');
    }

    private function registerAliases() {
        AliasLoader::getInstance()->alias('UserService', 'UNE\Facades\User');
        AliasLoader::getInstance()->alias('Review', 'UNE\Facades\Review');
        AliasLoader::getInstance()->alias('Comment', 'UNE\Facades\Comment');
        AliasLoader::getInstance()->alias('State', 'UNE\Facades\State');
    }

} 