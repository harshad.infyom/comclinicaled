<?php


namespace UNE\Repositories\Favorite;


use UNE\Models\Favorite;
use UNE\Repositories\AbstractRepository;

class FavoriteRepositoryEloquent extends AbstractRepository implements FavoriteRepository {
    protected $model;

    function __construct(Favorite $model) {
        $this->model = $model;
    }

} 