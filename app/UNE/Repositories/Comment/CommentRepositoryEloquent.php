<?php


namespace UNE\Repositories\Comment;


use UNE\Models\Comment;
use UNE\Repositories\AbstractRepository;

class CommentRepositoryEloquent extends AbstractRepository implements CommentRepository {

    protected $model;

    function __construct(Comment $model) {
        $this->model = $model;
    }


} 