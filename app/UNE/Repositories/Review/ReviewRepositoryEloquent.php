<?php


namespace UNE\Repositories\Review;


use stdClass;
use UNE\Models\Review;
use UNE\Repositories\AbstractRepository;

class ReviewRepositoryEloquent extends AbstractRepository implements ReviewRepository {
    protected $model;

    function __construct(Review $model) {
        $this->model = $model;
    }


    public function paginate($limit, $page, $sort = null){
//        $data['items'] = $this->model->orderBy($sort)->skip($limit * ($page - 1))->take($limit)->get();
//        $data['total'] = $this->model->count();
//
//        $result             = new StdClass;
//        $result->page       = $page;
//        $result->limit      = $limit;
//        $result->items      = $data['items'];
//        $result->totalItems = $this->model->count();


        $result = $this->model->orderBy($sort)->paginate($limit);
        return $result;
    }

    public function getSearchReviews($data, array $with = []){
        $page = $data['page'];
        $sort = $data['sort'];

        //REMOVE empty keys
        foreach($data as $key => $col){
            if($col == '' || $col == ' ' || strtolower($col) == 'all' || $key == 'page' || $key == 'sort'){
                unset($data[$key]);
            }
        }

        //BUILD Query
        $reviews = $this->make($with);

        foreach($data as $key => $value){
            $reviews = $reviews->where(function($q) use($key, $value){
                $q->where($key, $value);
            });
        }
        

        $reviews = $reviews->has('comments');

        //Order and Paginate
        $reviews = $reviews->orderBy($sort);
        $reviews = $reviews->paginate(10);

        //dd($reviews->toArray());

        return $reviews;




//        //$states = $this->states();
//
//        //IF State is selected let's get selected state results
//        if ($data[ 'state' ] != '') {
//            //Get STATE full name
//            $state = State::where('short', ( $data[ 'state' ] ))->first();
//            //$state   = $states[ $data[ 'state' ] ];
//
//
//            $reviews = $this->repo->getWhereMany('state', $state->name);
//        } else if ($data[ 'discipline' ] != '') {
//            //
//            $reviews = $this->repo->getWhereMany('service', $data[ 'discipline' ]);
//        }
//
//        if (isset( $reviews )) {
//            $reviews->orderBy($sort);
//            $reviews = $reviews->paginate(10);
//        } else {
//            //$reviews = $this->repo->paginate(10, $data['page'], $data['sort']);
//            //$reviews = \Paginator::make($reviews->items->toArray(), $reviews->totalItems, 10);
//            $reviews = $this->repo->paginate(10, $page, $data[ 'sort' ]);
//        }
//
//
//        //$reviews->setBaseUrl('state=IL');
//
//        return $reviews;
    }


} 