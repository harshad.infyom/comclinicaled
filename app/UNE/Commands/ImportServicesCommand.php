<?php namespace UNE\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ImportServicesCommand extends Command {

    private $path = '/home/comclinicaled/tblSiteReviews_Service_State_Site.csv';
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'import:services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will import service into DB from the file tblSiteReviews_Service_State_Site.csv';

    /**
     * Create a new command instance.
     *
     * @return \UNE\Commands\ImportServicesCommand
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        if (\File::exists($this->path)) {
            $states = \State::importExcelData($this->path);
            echo "Services imported!";
        } else {
            echo "The file doesn't exist." . $this->path;
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
