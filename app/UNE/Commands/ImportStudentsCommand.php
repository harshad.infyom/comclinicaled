<?php namespace UNE\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ImportStudentsCommand extends Command {

    private $path = '/home/comclinicaled/tblSiteReviews_Students.csv';
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'import:students';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will import student to DB from the file tblSiteReviews_Students.csv';

    /**
     * Create a new command instance.
     *
     * @return \UNE\Commands\ImportStudentsCommand
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {

        if (\File::exists($this->path)) {
            \DB::statement('TRUNCATE TABLE  users');

            $user = \Sentry::createUser([
                                           'email'      => 'sshekarsiri@gmail.com',
                                           'password'   => '1234',
                                           'activated'  => true,
                                           'first_name' => 'Shekar',
                                           'last_name'  => 'Siri',
                                           'prn'        => '132155321'
                                       ]);
            $admin = \Sentry::findGroupByName('Administrator');
            $user->addGroup($admin);

            $users = \UserService::importExcelData($this->path);
            echo "Students imported!";
        } else {
            echo "The file doesn't exist " . $this->path;
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array( //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
