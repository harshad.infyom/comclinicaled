<?php namespace UNE\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ImportCommentsCommand extends Command {
    private $path = '/home/comclinicaled/tblSiteReviews_CommentsAll.csv';
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:comments';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This will import comments into DB from the file tblSiteReviews_CommentsAll.csv';

    /**
     * Create a new command instance.
     *
     * @return \UNE\Commands\ImportCommentsCommand
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        if (\File::exists($this->path)) {
            $comments = \Comment::importExcelData($this->path);
            echo "Comments imported!";
        } else {
            echo "The file doesn't exist " . $this->path;
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
