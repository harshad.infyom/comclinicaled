<?php namespace UNE\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ImportReviewsCommand extends Command {
    private $path = '/home/comclinicaled/tblSiteReview_Sites_Service.csv';
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:reviews';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This will import comments into DB from the file tblSiteReview_Sites_Service.csv';

    /**
     * Create a new command instance.
     *
     * @return \UNE\Commands\ImportCommentsCommand
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        if (\File::exists($this->path)) {
            $reviews = \Review::importExcelData($this->path);
            echo "Reviews are imported!";
        } else {
            echo "The file doesn't exist " . $this->path;
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
