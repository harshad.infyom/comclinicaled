<?php


namespace UNE\Services;

use Excel;
use UNE\Models\State;
use UNE\Repositories\Review\ReviewRepository;

class ReviewService {

    private $repo;

    function __construct(ReviewRepository $repo) {
        $this->repo = $repo;
    }

    public function all() {
        $reviews = $this->repo->all();

        return $reviews;
    }

    public function create($data) {
        $review = $this->repo->create($data);

        return $review;
    }

    public function find($id, array $with = array()) {
        $review = $this->repo->find($id, $with);

        return $review;
    }

    public function findByState($state) {
        $reviews = $this->repo->getWhereMany('state', $state);

        return $reviews;
    }

    public function importExcelData($file) {
        $reviews = [];

        $rows = Excel::load($file)->get();

        foreach ($rows as $row) {
            $review = [];
            foreach ($row as $key => $cell) {
                $review[ $key ] = $cell ? $cell : '';
            }

            try {
                $review = $this->repo->firstOrCreate($review);
                array_push($reviews, $review);
            } catch (\Exception $e) {

            }
        }

        return $reviews;
    }

    public function getSearchReviews($data, array $with = []) {
        $reviews = $this->repo->getSearchReviews($data, $with);

        return $reviews;
    }

    public function states() {
        $states = [
            "AL" => "Alabama",
            "AK" => "Alaska",
            "AZ" => "Arizona",
            "AR" => "Arkansas",
            "CA" => "California",
            "CO" => "Colorado",
            "CT" => "Connecticut",
            "DE" => "Delaware",
            "DC" => "District Of Columbia",
            "FL" => "Florida",
            "GA" => "Georgia",
            "HI" => "Hawaii",
            "ID" => "Idaho",
            "IL" => "Illinois",
            "IN" => "Indiana",
            "IA" => "Iowa",
            "KS" => "Kansas",
            "KY" => "Kentucky",
            "LA" => "Louisiana",
            "ME" => "Maine",
            "MD" => "Maryland",
            "MA" => "Massachusetts",
            "MI" => "Michigan",
            "MN" => "Minnesota",
            "MS" => "Mississippi",
            "MO" => "Missouri",
            "MT" => "Montana",
            "NE" => "Nebraska",
            "NV" => "Nevada",
            "NH" => "New Hampshire",
            "NJ" => "New Jersey",
            "NM" => "New Mexico",
            "NY" => "New York",
            "NC" => "North Carolina",
            "ND" => "North Dakota",
            "OH" => "Ohio",
            "OK" => "Oklahoma",
            "OR" => "Oregon",
            "PA" => "Pennsylvania",
            "RI" => "Rhode Island",
            "SC" => "South Carolina",
            "SD" => "South Dakota",
            "TN" => "Tennessee",
            "TX" => "Texas",
            "UT" => "Utah",
            "VT" => "Vermont",
            "VA" => "Virginia",
            "WA" => "Washington",
            "WV" => "West Virginia",
            "WI" => "Wisconsin",
            "WY" => "Wyoming"
        ];

        return $states;
    }


}