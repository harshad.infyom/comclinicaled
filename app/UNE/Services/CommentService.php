<?php


namespace UNE\Services;


use Carbon\Carbon;
use Faker\Provider\DateTime;
use Maatwebsite\Excel\Facades\Excel;
use UNE\Repositories\Comment\CommentRepository;

class CommentService {

    private $repo;

    function __construct(CommentRepository $repo) {
        $this->repo = $repo;
    }

    public function all() {
        $comments = $this->repo->all();

        return $comments;
    }

    public function find($id) {
        $comment = $this->repo->find($id);

        return $comment;
    }

    public function create($data) {
        $comment = $this->repo->create($data);

        return $comment;
    }

    public function importExcelData($file) {

        $comments = [];

        $rows = Excel::load($file)->get();

        foreach ($rows as $row) {
            //dd($row);
            $comment = [];
            foreach ($row as $key => $cell) {
                $key = \Str::snake($key, '_');

                switch ($key) {
                    case 'start_date':
                    case 'end_date':
                        if($cell) {
                            $d               = str_replace("-", "-", $cell);
                            $cell            = Carbon::createFromFormat("d-M-y", $d);
                        }
                        $comment[ $key ] = $cell ? $cell : '';
                        break;


                    default:
                        $comment[ $key ] = $cell ? $cell : '';
                        break;
                }

            }

            try {
                $comm = $this->repo->getWhere('record_id', $comment[ 'record_id' ]);

                if (!$comm) {
                    //try {
                        $comment = $this->repo->firstOrCreate($comment);
                        array_push($comments, $comment);
                    //} catch (\Exception $e) {

                    //}
                }
            } catch (\Exception $e) {
                \Log::info('------------------------------------------------------------');
                \Log::info($comment);
                \Log::info($e->getMessage());
                \Log::info('------------------------------------------------------------');
            }

        }

        return $comments;
    }

} 