<?php


namespace UNE\Services;


use Maatwebsite\Excel\Facades\Excel;
use UNE\Models\Discipline;
use UNE\Repositories\State\StateRepository;

class StateService {

    private $repo;

    function __construct(StateRepository $repo) {
        $this->repo = $repo;
    }

    public function all(array $with = []) {
        $states = $this->repo->all($with);

        return $states;
    }

    public function find($id, array $with = []) {
        $state = $this->repo->find($id, $with);

        return $state;
    }


    public function importExcelData($file) {
        $reviews = [];

        $rows = Excel::load($file)->get();

        foreach ($rows as $row) {
            $review = [];
            foreach ($row as $key => $cell) {
                $review[ $key ] = $cell ? $cell : '';
            }

            try {
                //$review = $this->repo->firstOrCreate($review);
                array_push($reviews, $review);
            } catch (\Exception $e) {

            }
        }

        $tmp = array();

        foreach ($reviews as $arg) {
            $tmp[ $arg[ 'name' ] ][ ] = $arg[ 'servicename' ];
        }

        $states = array();

        foreach ($tmp as $type => $labels) {
            $states[ ] = array(
                'name'        => $type,
                'disciplines' => $labels
            );
        }

        $available_disciplines = [];
        foreach ($states as $state_arr) {
            $state = $this->repo->getWhere('name', $state_arr[ 'name' ]);

            if ($state) {
                $ids = [1];
                foreach ($state_arr[ 'disciplines' ] as $dis_name) {
                    $discipline = Discipline::where('name', $dis_name)->first();
                    array_push($ids, $discipline->id);
                    array_push($available_disciplines, $discipline->id);
                }
                $state->disciplines()->sync($ids);
            }
        }

        $available_disciplines = array_unique($available_disciplines);
        $state = $this->repo->find(1);
        $state->disciplines()->sync($available_disciplines);


        //dd($available_disciplines);

        return $reviews;
    }


} 