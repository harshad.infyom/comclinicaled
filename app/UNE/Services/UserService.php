<?php


namespace UNE\Services;


use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Cartalyst\Sentry\Groups\GroupNotFoundException;
use Illuminate\Support\Facades\Log;
use Laracasts\Flash\Flash;
use Maatwebsite\Excel\Facades\Excel;
use UNE\Repositories\Favorite\FavoriteRepository;
use UNE\Repositories\Review\ReviewRepository;
use UNE\Repositories\User\ProfileRepository;
use UNE\Repositories\User\UserRepository;

class UserService {

    private $repo;
    /**
     * @var \UNE\Repositories\User\ProfileRepository
     */
    private $profile_repo;
    /**
     * @var \UNE\Repositories\Favorite\FavoriteRepository
     */
    private $favorite_repo;
    /**
     * @var \UNE\Repositories\Review\ReviewRepository
     */
    private $review_repo;

    function __construct(UserRepository $repo, ProfileRepository $profile_repo, ReviewRepository $review_repo) {
        $this->repo         = $repo;
        $this->profile_repo = $profile_repo;
        $this->review_repo  = $review_repo;
    }

    public function create($data) {
        $user = Sentry::createUser($data);

        return $user;
    }

    public function update($data) {
        $user = Sentry::getUser();
        //dd($data);

        $user = $this->repo->update($user->id, $data);
        //dd($user);
        return $user;
    }


    public function addFavourite($siteID) {
        $user = Sentry::getUser();

        //$site = $this->review_repo->find($siteID);
        //$favourites = $user->favorites();

        $user->favorites()->detach($siteID);
        $user->favorites()->attach($siteID, ['prn' => $user->prn ]);

        //Flash::overlay('Done');
        return;
    }

    public function getFavourites() {
        $user       = $user = Sentry::getUser();
        $favourites = $user->favorites()->paginate(10);

        return $favourites;
    }


    public function importExcelData($file) {
        $users = [];

        $rows = Excel::load($file)->get();

        foreach ($rows as $row) {
            $data = [];

            $data[ 'password' ]  = "1234";
            $data[ 'email' ]     = str_random(8) . '@example.com';
            $data[ 'activated' ] = 1;

            unset($row['StudentName']);
            unset($row['MiddleInitial']);

            //print_r($row);

            foreach ($row as $key => $cell) {
                //                if ($key == "permissions") {
                //                    //JUST Checking if group exists
                //                    try {
                //                        $group = Sentry::findGroupByName($cell);
                //                    } catch (GroupNotFoundException $e) {
                //
                //                    }
                //                } else {
                //                    $user[ $key ] = $cell ? "$cell" : "";
                //                }

                //ADD Column data from Excel Sheet
                if($key == 'PRN') {
                    $key = 'prn';
                } else {
                    $key = \Str::snake($key, '_');
                }


                $data[ $key ] = $cell ? "$cell" : "";

                //
            }
            //dd($data);

            try {
                $user = \Sentry::getUserProvider()->create($data);
                //$user->addGroup($group);
                array_push($users, $user);
            } catch (\Exception $e) {
                Log::info($e->getMessage());
            }


            //Add Member Group
            //            if (isset($user)) {
            //                try {
            //                    $group = Sentry::findGroupByName('Member');
            //                    $user->addGroup($group);
            //                } catch (\Exception $e) {
            //                    Log::info($e->getMessage());
            //                }
            //            }
        }


        return $users;
    }

} 