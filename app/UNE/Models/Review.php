<?php


namespace UNE\Models;


use Illuminate\Database\Eloquent\Model;

class Review extends Model {

    protected $table = 'reviews';
    protected $fillable = ['site_id', 'name', 'street', 'street2', 'city', 'state', 'zipcode', 'service'];

    public function comments() {
        return $this->hasMany('UNE\Models\Comment', 'site_id', 'site_id');
    }
} 