<?php


namespace UNE\Models;


use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model {
    protected $table = 'users_profiles';
    protected $fillable = ['first_name', 'last_name', 'user_id'];

    public function user(){
        return $this->belongsTo('UNE\Models\User');
    }

    public function full_name(){
        $full_name = $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
        return ucfirst($full_name);
    }

} 