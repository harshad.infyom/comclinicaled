<?php


namespace UNE\Models;


use Illuminate\Database\Eloquent\Model;

class Discipline extends Model {

    protected $fillable = ['name'];
    public $timestamps = false;

    public function state() {
        return $this->belongsTo('UNE\Models\State');
    }

    public function states(){
        return $this->belongsToMany('UNE\Models\State');
    }
} 