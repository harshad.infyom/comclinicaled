<?php


namespace UNE\Models;


class User extends \Cartalyst\Sentry\Users\Eloquent\User {
    protected $fillable = ['email', 'password', 'activated', 'first_name', 'last_name', 'prn'];

    public function profile(){
        return $this->hasOne('UNE\Models\UserProfile');
    }

    public function favorites(){
        return $this->belongsToMany('UNE\Models\Review', 'users_favorites', 'user_id', 'site_id')->withTimestamps();
    }

    public function full_name(){
        $full_name = $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
        return ucfirst($full_name);
    }

} 