<?php


namespace UNE\Models;


use Illuminate\Database\Eloquent\Model;

class State extends Model {
    protected $fillable = ['short', 'name'];
    public $timestamps = false;

    public function disciplines(){
        return $this->belongsToMany('UNE\Models\Discipline');
    }
} 