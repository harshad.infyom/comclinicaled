<?php


namespace UNE\Models;


use Illuminate\Database\Eloquent\Model;

class Favorite extends Model {
    protected $table = 'users_favorites';
} 