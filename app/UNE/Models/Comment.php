<?php


namespace UNE\Models;


use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    protected $table = 'comments';
    protected $fillable = ['record_id', 'eval_id', 'site_id', 'service_name', 'start_date', 'end_date', 'strengths', 'improvements', 'typical_day', 'overall'];

    public function review(){
        return $this->belongsTo('UNE\Models\Review', 'site_id', 'site_id');
    }
} 