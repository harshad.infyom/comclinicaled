<?php


namespace UNE;


use Illuminate\Support\ServiceProvider;

class UNEServiceProvider extends ServiceProvider {
    protected $app;

    function __construct($app) {
        $this->app = $app;
    }


    public function boot() {

    }

    public function register() {
        $this->app->register('UNE\Repositories\RepositoryServiceProvider');
    }
}